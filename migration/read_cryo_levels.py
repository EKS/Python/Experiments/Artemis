# -*- coding: utf-8 -*-
"""
Read some paramters from cryogenic monitor E5025.

Migrated from: D:\Artemis\Projektvorlage\Application VI\AmbientAll_FoV.vi

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import datetime
import re
import serial
import time

readout_interval = 1  # Unit: s , recommended: 1800s for monitoring and 60s during filling.
print('Press Ctrl+c to stop.')
try:
    with serial.Serial('com3',
                       baudrate=9600,
                       bytesize=serial.EIGHTBITS,
                       parity=serial.PARITY_NONE,
                       stopbits=serial.STOPBITS_ONE,
                       xonxoff=True,
                       timeout=60
                       ) as ser:
        iteration_counter = 0
        while True:
            iteration_counter += 1
            # print(f'Interation: {iteration_counter}')
            ser.write('W\n'.encode('utf-8'))
            # time.sleep(3)
            line = ser.readline().decode('utf-8')
            # print(f'Response: {line}')
            items = re.split(',', line)
            if len(items) > 0:
                if re.fullmatch('E5025', items[0]):
                    tmp = re.split('P1D0889m', items[2])[1]
                    level_liquid_helium = int(re.split('%', tmp)[0]) / 100.
                    level_liquid_nitrogen = re.split('P2D####m', items[3])[1]
                    print(f'{datetime.datetime.now()} Levels: helium={level_liquid_helium * 100.}, nitrogen={level_liquid_nitrogen}')
            # time.sleep(3)
            time.sleep(readout_interval)
except KeyboardInterrupt:
    print('Script stopped by user (Ctrl+c)')
