#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Desription

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import aioconsole
import asyncio
import re

termination = '\r'  # '\n' or '\r\n'


async def tcp_echo_client():
    try:
        # reader, writer = await asyncio.open_connection('eeldev002', 8000)
        reader, writer = await asyncio.open_connection('atpdev011', 9003)
        try:
            while True:
                message = await aioconsole.ainput('Command or "exit"> ')
                if re.search('exit', message):
                    exit(0)
                message += termination
                writer.write(message.upper().encode())
                await writer.drain()
                if re.search('\?', message):
                    line = await reader.readuntil(termination.encode('utf-8'))
                    response = re.split(termination, line.decode())[0]
                    print(f'Received: {line}')
                    if response[0] == '\x13':
                        response = response[1:-1]
                    print(f'Response: {response}')
                else:
                    print('Command is query. Nmo response to read.')
                    continue
        except KeyboardInterrupt:
            print('Stopped by user <Ctrl+c>')
    except Exception as e:
        print(f'Exception cought: {e}')
    finally:
        print('Close the connection')
        writer.close()
        await writer.wait_closed()
    pass

asyncio.run(tcp_echo_client())
