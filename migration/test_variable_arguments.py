#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 15:27:46 2024

@author: brand
"""
import random
import re

tic_object = {
    'Node': 901,
    'TIC Status': 902,
    'Turbo Pump': 904,
    'Turbo speed': 905,
    'Turbo power': 906,
    'Turbo normal': 907,
    'Turbo satandby': 908,
    'Turbo cycle time': 909,
    'Backing Pump': 910,
    'Backing speed': 911,
    'Backing power': 912,
    'Gauge 1': 913,
    'Gauge 2': 914,
    'Gauge 3': 915,
    'Relay 1': 916,
    'Relay 2': 917,
    'Relay 3': 918,
    'PS Temperature': 919,
    'Internal Temperature': 920,
    'Analogue out': 921,
    'Externalvalve': 922,
    'Heater band': 923,
    'ExternalCooler': 924,
    'Display contrast': 925,
    'Configuration Operations': 926,
    'Lock': 928,
    'Pressure Units': 929,
    'PC comms': 930,
    'Default screen': 931,
    'Fixed/Float ASG': 932,
    'System (TIC and TC only)': 933,
    'Gauge 4': 934,
    'Gauge 5': 935,
    'Gauge 6': 936,
    'Relay 4': 937,
    'Relay 5': 938,
    'Relay 6': 939,
    'Gauge Values': 940}
tic_object_by_id = dict(zip(tic_object.values(), tic_object.keys()))
tic_response_code = {
    0: 'No error',
    1: 'Invalid command for object ID',
    2: 'Invalid query/command',
    3: 'Missing parameter',
    4: 'Parameter out of range',
    5: 'Invalid command in current state - e.g. serial command to start or stop when in parallel control mode',
    6: 'Data checksum error',
    7: 'EEPROM read or write error',
    8: 'Operation took too long',
    9: 'Invalid config ID'}
tic_priority = {
    0: 'OK',
    1: 'Warning',
    2: 'Alarm',
    3: 'Alarm'}
tic_alert_id = {
    0: 'No Alert',
    1: 'ADC Fault',
    2: 'ADC Not Ready',
    3: 'Over Range',
    4: 'Under Range',
    5: 'ADC Invalid',
    6: 'No Gauge',
    7: 'Unknown',
    8: 'Not Supported',
    9: 'New ID',
    10: 'Over Range',
    11: 'Under Range',
    12: 'Over Range',
    13: 'Ion Em Timeout',
    14: 'Not Struck',
    15: 'Filament Fail',
    16: 'Mag Fail',
    17: 'Striker Fail',
    18: 'Not Struck',
    19: 'Filament Fail',
    20: 'Cal Error',
    21: 'Initialising',
    22: 'Emission Error',
    23: 'Over Pressure',
    24: 'ASG Cant Zero',
    25: 'RampUp Timeout',
    26: 'Droop Timeout',
    27: 'Run Hours High',
    28: 'SC Interlock',
    29: 'ID Volts Error',
    30: 'Serial ID Fail',
    31: 'Upload Active',
    32: 'DX Fault',
    33: 'Temp Alert',
    34: 'SYSI Inhibit',
    35: 'Ext Inhibit',
    36: 'Temp Inhibit',
    37: 'No Reading',
    38: 'No Message',
    39: 'NOV Failure',
    40: 'Upload Timeout',
    41: 'Download Failed',
    42: 'No Tube',
    43: 'Use Gauges 4-6',
    44: 'Degas Inhibited',
    45: 'IGC Inhibited',
    46: 'Brownout/Short',
    47: 'Service due'}
tic_snvt = {
    'PRESSURE': 59,  # float (Pascals only)
    'VOLTAGE': 66,  # float
    'PERCENT': 81}
tic_command_list = {
    'Device Off': 0,
    'Device On': 1,
    'Gauge Off': 0,
    'Gauge On': 1,
    'Gauge New_Id': 2,
    'Gauge Zero': 3,
    'Gauge Cal': 4,
    'Gauge Degas': 5,
    'Load Defaults': 576,
    'Upload': 0,
    'Download': 1}
tic_state = {
    0: 'Off',
    1: 'Off Going On',
    2: 'On Going Off Shutdown',
    3: 'On Going Off Normal',
    4: 'On'}
tic_active_gauge_state = {
    0: 'Gauge Not connected',
    1: 'Gauge Connected',
    2: 'New Gauge Id',
    3: 'Gauge Change',
    4: 'Gauge In Alert',
    5: 'Off',
    6: 'Striking',
    7: 'Initialising',
    8: 'Calibrating',
    9: 'Zeroing',
    10: 'Degassing',
    11: 'On',
    12: 'Inhibited'}
tic_full_pump_states = {
    0: 'Stopped',
    1: 'Starting Delay',
    2: 'Stopping Short Delay',
    3: 'Stopping Normal Delay',
    4: 'Running',
    5: 'Accelerating',
    6: 'Fault Braking',
    7: 'Braking'}
tic_gas_type = {
    'Nitrogen': 0,
    'Helium': 1,
    'Argon': 2,
    'Carbon Dioxide': 3,
    'Neon': 4,
    'Krypton': 5,
    'Voltage': 6}
tic_gas_type_by_id = dict(zip(tic_gas_type.values(), tic_gas_type.keys()))
tic_filter = {
    'Filter Off': 0,
    'Filter On': 1}
tic_filter_by_id = dict(zip(tic_filter.values(), tic_filter.keys()))
tic_gauge_type = {
    'Unknown Device': 0,
    'No Device': 1,
    'EXP_CM': 2,
    'EXP_STD': 3,
    'CMAN_S': 4,
    'CMAN_D': 5,
    'TURBO': 6,
    'APGM': 7,
    'APGL': 8,
    'APGXM': 9,
    'APGXH': 10,
    'APGXL': 11,
    'ATCA': 12,
    'ATCD': 13,
    'ATCM ': 14,
    'WRG': 15,
    'AIMC': 16,
    'AIMN': 17,
    'AIMS': 18,
    'AIMX': 19,
    'AIGC_I2R': 20,
    'AIGC_2FIL': 21,
    'ION_EB': 22,
    'AIGXS': 23,
    'USER': 24,
    'ASG': 25}
tic_gauge_type_by_id = dict(zip(tic_gauge_type.values(), tic_gauge_type.keys()))
tic_pump_tpye = {
    'No Pump': 0,
    'EXDC Pump': 1,
    'EXT75DX Pump': 3,
    'EXT255DX': 4,
    'Mains Backing Pump': 8,
    'Serial Pump': 9,
    'nEXT - 485': 10,
    'nEXT - 232': 11,
    'nXDS': 12,
    'Not yet identified': 0}
tic_pump_tpye_by_id = dict(zip(tic_pump_tpye.values(), tic_pump_tpye.keys()))
tic_operation = {
    'GENERAL COMMAND': '!C',
    'SETUP COMMAND': '!S',
    'QUERY SETUP': '?S',
    'QUERY VALUE': '?V'}


def tic_build_command(operation, object_id, *argv):
    # Example: !S913 0;0;0
    command = operation + str(object_id)
    # print(f'len(argv): {len(argv)} argv: {argv}')
    if len(argv) > 0:
        command += ' '
        for arg in argv:
            command += str(arg) + ';'
        command = command[:-1]
    # print(f'Command: {command}')
    return command


def tic_parse(response):
    # print(f'Response: {response}')
    result = status = None
    # if re.search('=', response):  # Vaule returned
    if response[0] == '=':  # Vaule returned
        result = re.split(' ', response, 1)
        result_list = re.split(';', result[1])
        return result_list, None
    if response[0] == '*':  # Vaule returned
        status = re.split(' ', response, 1)
        return None, int(status[1])
    else:
        raise Exception('TIC response is invalid.')
    pass


# Some objects have more than one setup, for these objects the config type is sent and returned as the first parameter in the data field.
# Refer to Notes to Objcect ID.
test_case = 3
match test_case:
    case 0:  # Build command
        config_type = 7
        command = tic_build_command(tic_operation['SETUP COMMAND'], tic_object['Gauge 1'], config_type, tic_gas_type['Argon'], tic_filter['Filter Off'])
        print(f'Command: {command}')
    case 1:  # Parse response
        try:
            response = '*TICxxx; SW 1.0; Ser 123; PIC SW 0.1'
            value, errno = tic_parse(response)
            print(f'Response result: {value}')
        except Exception as e:
            print(f'Response exception: {e}')
    case 2:  # Parse error response
        try:
            response = '*S12345 1'
            value, errno = tic_parse(response)
            print(f'Response result: {errno} ({tic_response_code[errno]})')
        except Exception as e:
            print(f'Response exception: {e}')
    case 3:  # Query pressure as implemented in LV
        try:
            gases = []
            gauge_types = []
            pressures = []
            units = []
            gauge_states = []
            alerts = []
            priorities = []
            oid_gas_filter = [
                (tic_object['Gauge 1'], tic_gas_type['Argon'], tic_filter['Filter Off']),
                (tic_object['Gauge 2'], tic_gas_type['Nitrogen'], tic_filter['Filter Off']),
                (tic_object['Gauge 3'], tic_gas_type['Helium'], tic_filter['Filter Off']),
                (tic_object['Gauge 4'], tic_gas_type['Neon'], tic_filter['Filter Off'])]
            for oid, gas, f in oid_gas_filter:
                # Write command only. Set gauge setup - gas type (volt); filter on/off:  Config type = 7
                config_type = 7
                command = tic_build_command(tic_operation['SETUP COMMAND'], oid, config_type, gas, f)
                # print(f'Set gas type and filter. Command: {command}')
                # Write command and read response. 5: Read gauge type – e.g. AIMX: Config type = 5
                config_type = 5
                command = tic_build_command(tic_operation['QUERY SETUP'], oid, config_type)
                # print(f'Query gauge type. Command: {command}')
                # response = '=Vnnnnn' + ' 7'
                response = '=Vnnnnn' + ' ' + str(random.randint(0, 15))
                result_list, errno = tic_parse(response)
                gauge_type = tic_gauge_type_by_id[int(result_list[0])]
                tic_build_command(tic_operation['QUERY VALUE'], oid)
                # print(f'Query pressuere value and status. Command: {command}')
                # response = '=Vnnnnn' + ' 1.23E-3;hPa;12;47;3'
                response = '=Vnnnnn' + ' ' + str(random.random()) + ';hPa;' + str(random.randint(0, 12)) + ';' + str(random.randint(0, 47)) + ';' + str(random.randint(0, 3))
                result_list, errno = tic_parse(response)
                pressure = float(result_list[0]) / 100.
                unit = result_list[1]
                gauge_state = tic_active_gauge_state[int(result_list[2])]
                alert = tic_alert_id[int(result_list[3])]
                priority = tic_priority[int(result_list[4])]
                print(f'{tic_object_by_id[oid]} ({tic_gas_type_by_id[gas]}) P={pressure:.3e}{unit}|{gauge_type}|{gauge_state}|{alert}|{priority}')
                gases.append(tic_gas_type_by_id[gas])
                gauge_types.append(gauge_type)
                pressures.append(pressure)
                units.append(unit)
                gauge_states.append(gauge_state)
                alerts.append(alerts)
                priorities.append(priority)
        except Exception as e:
            print(f'Response exception: {e}')
