#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test sending eMail via GSI Excahnge server using asyncio.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
from email.message import EmailMessage
import aiosmtplib
import json
import aiofiles

import nest_asyncio
nest_asyncio.apply()


async def send_status_mail(infos):
    message = EmailMessage()
    message['Subject'] = infos['Subject']
    message['From'] = infos['From']
    message['To'] = infos['To']
    try:
        message['Cc'] = ",".join(infos["Cc"])
        message['Bcc'] = ",".join(infos["Bcc"])
    except KeyError:
        pass
    content = 'Testmail from Python using asycio'
    message.set_content(content)
    print(f'Sending eMail Message: {message}', flush=True)
    await aiosmtplib.send(message,
                          hostname='smtp.gsi.de',
                          port=25,
                          use_tls=False,
                          start_tls=None
                          )
    pass


async def read_email_configuration():
    async with aiofiles.open('../config/email.json', mode='r') as f:
        json_content = await f.read()
        # print(f'eMail configuration from json file: {json_content}')
        try:
            infos = json.loads(json_content)
            print(f'eMail configuration from json: {infos}')
            print(f'Subject: {infos["Subject"]}')
            print(f'eMail From: {infos["From"]}')
            print(f'eMail To: {infos["To"]}')
            try:
                print(f'eMail Cc: {",".join(infos["Cc"])}')
                print(f'eMail Bcc: {",".join(infos["Bcc"])}')
            except KeyError:
                pass
        except Exception as e:
            print(f'read_email_configuration: Exception cought: {e}')
        return infos


async def main():
    infos = await read_email_configuration()
    await send_status_mail(infos)
    pass


asyncio.run(main())
