#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Send anonymous eMail using subprocess on Windows or Linux.

Linux:
    - Make sure that [postfix](https://www.postfix.org/) is installed the corresponding service is eabnled and started on your local system.
    - In `/etc/postfix/main.cf` you need to set following entries:
        myhostname = <hostname>.gsi.de`
        relayhost = [smtp.gsi.de]:25`
        - When modified you need to restat the service:
    - sudo systemctl restart postfix`
Windows: No additional preparation necessary.

@author: H.Brand@gsi.de
"""
import os
import subprocess
import sys

# Change to the "Selector" event loop if platform is Windows
if sys.platform.lower() == "win32" or os.name.lower() == "nt":
    content = '"Testmail from Python using PowerShell on Windows."'
    ps_script = """
    $PSEmailServer="smtp.gsi.de"
    Send-MailMessage -Port 25 -to "H.Brand@gsi.de" -from "Artemis@gsi.de" -Subject "Testmail from Windows" -body """
    ps_script += content
    process = subprocess.Popen(["powershell.exe", ps_script], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
else:
    content = 'Testmail from Python using mail and postfix on Linux.'
    process = subprocess.Popen(['echo ' + content + '| /usr/bin/mail -r Artemis@gsi.de -s "Testmail from Linux" H.Brand@gsi.de'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout, stderr = process.communicate()
print(f'stdout: {stdout}\nstderr: {stderr}')
