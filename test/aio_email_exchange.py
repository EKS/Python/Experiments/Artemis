#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test sending eMail via GSI Excahnge server using asyncio.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
from email.message import EmailMessage
import aiosmtplib
import getpass
import nest_asyncio
import sys

nest_asyncio.apply()


async def send_status_mail(email_user, email_password):
    message = EmailMessage()
    message['From'] = 'H.Brand@gsi.de'
    message['To'] = 'H.Brand@gsi.de'
    message['Cc'] = 'H.Brand@gsi.de'
    message['Subject'] = 'Testmail'
    content = 'Testmail from Python via Exchange'
    message.set_content(content)
    print(f'Sending eMail Message: {message}', flush=True)
    await aiosmtplib.send(message,
                          hostname='email.gsi.de',
                          port=587,
                          username=email_user,
                          password=email_password,
                          use_tls=False,
                          start_tls=None
                          )


email_user = input('eMail user> ')
if len(email_user) == 0:
    print('No eMail user specified.')
    sys.exit(-1)
email_password = None
if email_user:  # Password is re required
    print('Set password for eMail')
    email_password = getpass.getpass()
    if len(email_password) == 0:
        print('No pasword specified.')
        sys.exit(-1)
asyncio.run(send_status_mail(email_user, email_password))
