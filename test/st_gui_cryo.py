#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Streamlit GUI for the Artemis Cryo System.

It utilizes MQTT client for publishing and subscribing to topics and
display in WebGUI using Streamlit.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Acknowledgements
----------------
- The MQTT part is inpired by http://www.steves-internet-guide.com

"""
import datetime
import json
import numpy as np
import os
import pandas as pd
import platform
import re
import streamlit as st
from streamlit_echarts import st_echarts
import sys
import time
import paho.mqtt.client as mqtt

st.title('Artemis Cryo System (Streamlit GUI)')
st.info('This Web-GUI is a prototype design to illustrate streamlit to Artemis users.', icon="ℹ️")
st.info('Use MQTT-Explorer (or other tools) to change set_readout_interval.', icon="ℹ️")

broker = 'ee-raspi1001.gsi.de'
client = None

hostname = platform.node()
# client_name = hostname + '_' + re.split('.py', os.path.basename(__file__))[0]
client_name = re.split('.py', os.path.basename(__file__))[0]
date_time = re.split(' ', str(datetime.datetime.now()))
client_id = client_name + '_' + date_time[0] + '_' + date_time[1]

st_status_info = st.empty().text('Logging messages become displayed here.')
st_broker_status = st.empty().text('MQTT broker status will be displayed here.')
st_connection_status = st.empty().text('Connected status will be displayed here.')

st.divider()
st_e5025_idn = st.empty().text('IDN of E5025 will be displayed here.')
st_e5025_iteration_counter = st.empty().text('Iteration counter will be displayed here.')
st_e5025_readout_interval = st.empty().text('readout_interval will be displayed here.')
st_e5025_set_readout_interval = st.empty().text('set_readout_interval will be displayed here.')
st.divider()
st_e5025_readout_time = st.empty().text('readout_time will be displayed here.')
st_level_liquid_helium = st.empty().text('level_liquid_helium will be displayed here.')
st_level_liquid_helium_bar = st.progress(0, text='Liquid Helium Level')
st_level_liquid_nitrogen = st.empty().text('level_liquid_nitrogen will be displayed here.')
st_level_liquid_nitrogen_bar = st.progress(0, text='Liquid Nitrogen Level')

st.divider()
st_ks2700_idn = st.empty().text('IDN of KS2700 will be displayed here.')
st_ks2700_iteration_counter = st.empty().text('Iteration counter will be displayed here.')
st_ks2700_readout_interval = st.empty().text('readout_interval will be displayed here.')
st_ks2700_set_readout_interval = st.empty().text('set_readout_interval will be displayed here.')
st.divider()
st_ks2700_readout_time = st.empty().text('readout_time will be displayed here.')
st_temperatures = st.empty().text('temperatures will be displayed here.')
st_temperature_0 = st.empty().text('temperature_0 will be displayed here.')
st_temperature_1 = st.empty().text('temperature_1 will be displayed here.')
st_temperature_2 = st.empty().text('temperature_2 will be displayed here.')
st_temperature_3 = st.empty().text('temperature_3 will be displayed here.')
st.divider()
st_tic_idn = st.empty().text('IDN of tic will be displayed here.')
st_tic_iteration_counter = st.empty().text('Iteration counter will be displayed here.')
st_tic_readout_interval = st.empty().text('readout_interval will be displayed here.')
st_tic_set_readout_interval = st.empty().text('set_readout_interval will be displayed here.')
st.divider()
st_tic_readout_time = st.empty().text('readout_time will be displayed here.')
st_pressures = st.empty().text('pressures will be displayed here.')
st_pressure_0 = st.empty().text('pressure_0 will be displayed here.')
st_gauge_0 = st.empty().text('gauge_0 will be displayed here.')
st_pressure_1 = st.empty().text('pressure_1 will be displayed here.')
st_gauge_1 = st.empty().text('gauge_1 will be displayed here.')
st_pressure_2 = st.empty().text('pressure_2 will be displayed here.')
st_gauge_2 = st.empty().text('gauge_2 will be displayed here.')
st_pressure_3 = st.empty().text('pressure_3 will be displayed here.')
st_gauge_3 = st.empty().text('gauge_3 will be displayed here.')


def on_log(client, userdata, level, buf):
    """Log buffer if callback is assigned."""
    st_status_info(f'log: {buf}')
    # print('log: ' + buf)
    pass


def on_connect(client, userdata, flags, rc):
    """
    Handle broker connected callback.

    Publish all topic once.
    Subscribe to desired topics.
    """
    st.empty().text('temperatures will be displayed here.')
    if rc == 0:
        client.connected_flag = True
        # print(f'MQTT broker {broker} conneced with rc={str(rc)}.')
        st_broker_status.text(f'MQTT broker {broker} conneced with rc={str(rc)}.')
        subscribe_all(client)
    else:
        print(f'Bad connection to MQTT broker {broker} , returned code = {rc}')
        # st_status_info.text(f'Bad connection to MQTT broker {broker} , returned code = {rc}')
        pass
    pass


def subscribe_all(client):
    """Subscribe to all topics."""
    rc, mid = client.subscribe('EELNBG014/Cryo')
    print(f'Subscribing to: EELNBG014/Cryo returned rc = {rc} mid = {mid}')
    st_status_info.text(f'Subscribing to: EELNBG014/Cryo returned rc = {rc} mid = {mid}')
    rc, mid = client.subscribe('EELNBG014/Cryo' + '/#')
    print(f'Subscribing to: EELNBG014/Cryo/#" returned rc = {rc} mid = {mid}')
    st_status_info.text(f'Subscribing to: EELNBG014/Cryo/#" returned rc = {rc} mid = {mid}')
    pass


def on_disconnect(client, userdata, flags, rc=0):
    """Handle broker disconnected callback."""
    # client.connected_flag = False
    st_status_info.text(f'Disconnected st_set_readout_interval_inputfrom MQTT broker {broker}. Result code = {(rc)}')
    st_broker_status.text(f'Disonnected from MQTT broker {broker}.')
    # client.loop_stop()
    pass


def on_publish(client, userdata, mid):
    """Handle publish callback."""
    st_status_info.text(f'Client published message ID = {mid}')
    pass


def on_message(client, userdata, msg):
    """
    Handle message received callback.

    Decode received message data and insert into command processor queue.
    """
    global set_readout_interval_input_previous
    global temperature_data
    topic = msg.topic
    payload = str(msg.payload.decode('utf-8', 'ignore'))
    # print(f'Message received. Topic: {topic} Payload: {payload}')
    st_status_info.text(f'Message received. Topic: {topic} Payload: {payload}')
    match topic:
        case 'EELNBG014/Cryo':
            st_connection_status.text(f'Connection status of EELNBG014/Cryo: {payload}')
        case 'EELNBG014/Cryo/e5025/idn':
            st_e5025_idn.text(f'e5025_idn={payload}')
        case 'EELNBG014/Cimport pandas as pdryo/e5025/iteration_counter':
            st_e5025_iteration_counter.text(f'e5025_iteration_counter={payload}')
        case 'Est_temperatureELNBG014/Cryo/e5025/set_readout_interval_ssudo dsmc incr':
            st_e5025_set_readout_interval.text(f'e5025_set_readout_interval={payload}')
        case 'EELNBG014/Cryo/e5025/readout_interval_s':
            st_e5025_readout_interval.text(f'e5025_readout_interval={payload}')
        case 'EELNBG014/Cryo/e5025/readout_time':
            st_e5025_readout_time.text(f'e5025_readout_time={payload}')
        case 'EELNBG014/Cryo/level_liquid_helium':
            st_level_liquid_helium.text(f'level_liquid_helium={payload}')
            try:
                st_level_liquid_helium_bar.progress(float(payload), text='Liquid Helium Level')
            except Exception:
                pass
        case 'EELNBG014/Cryo/level_liquid_nitrogen':
            st_level_liquid_nitrogen.text(f'level_liquid_nitrogen={payload}')
            try:
                st_level_liquid_nitrogen_bar.progress(float(payload), text='Liquid Nitrogen Level')
            except Exception:
                pass
        case 'EELNBG014/Cryo/ks2700/idn':
            st_ks2700_idn.text(f'ks2700_idn={payload}')
        case 'EELNBG014/Cryo/ks2700/iteration_counter':
            st_ks2700_iteration_counter.text(f'ks2700_iteration_counter={payload}')
        case 'EELNBG014/Cryo/ks2700/set_readout_interval_s':
            st_ks2700_set_readout_interval.text(f'ks2700_set_readout_interval={payload}')
        case 'EELNBG014/Cryo/ks2700/readout_interval_s':
            st_ks2700_readout_interval.text(f'ks2700_readout_interval={payload}')
        case 'EELNBG014/Cryo/ks2700/readout_time':
            st_ks2700_readout_time.text(f'ks2700_readout_time={payload}')
        case 'EELNBG014/Cryo/temperatures':
            temperature_data = json.loads(payload)
            temperature_chart_data = pd.DataFrame(temperature_data, columns=["T /K"])
            st_temperatures.bar_chart(temperature_chart_data)
        case 'EELNBG014/Cryo/temperatures/0':
            st_temperature_0.text(f'temperature_0={payload}')
        case 'EELNBG014/Cryo/temperatures/1':
            st_temperature_1.text(f'temperature_1={payload}')
        case 'EELNBG014/Cryo/temperatures/2':
            st_temperature_2.text(f'temperature_2={payload}')
        case 'EELNBG014/Cryo/temperatures/3':
            st_temperature_3.text(f'temperature_3={payload}')
        case 'EELNBG014/Cryo/tic/idn':
            st_tic_idn.text(f'tic_idn={payload}')
        case 'EELNBG014/Cryo/tic/iteration_counter':
            st_tic_iteration_counter.text(f'tic_iteration_counter={payload}')
        case 'EELNBG014/Cryo/tic/set_readout_interval_s':
            st_tic_set_readout_interval.text(f'tic_set_readout_interval={payload}')
        case 'EELNBG014/Cryo/tic/readout_interval_s':
            st_tic_readout_interval.text(f'tic_readout_interval={payload}')
        case 'EELNBG014/Cryo/tic/readout_time':
            st_tic_readout_time.text(f'tic_readout_time={payload}')
        case 'EELNBG014/Cryo/pressures':
            pressure_data = json.loads(payload)
            pressure_chart_data = pd.DataFrame(pressure_data, columns=["T /K"])
            st_pressures.bar_chart(pressure_chart_data)
        case 'EELNBG014/Cryo/pressures/0':
            st_pressure_0.text(f'pressure_0={payload}')
        case 'EELNBG014/Cryo/pressures/1':
            st_pressure_1.text(f'pressure_1={payload}')
        case 'EELNBG014/Cryo/pressures/2':
            st_pressure_2.text(f'pressure_2={payload}')
        case 'EELNBG014/Cryo/pressures/3':
            st_pressure_3.text(f'pressure_3={payload}')
        case 'EELNBG014/Cryo/tic/gauges/0':
            st_gauge_0.text(f'gauge_0={payload}')
        case 'EELNBG014/Cryo/tic/gauges/1':
            st_gauge_1.text(f'gauge_1={payload}')
        case 'EELNBG014/Cryo/tic/gauges/2':
            st_gauge_2.text(f'gauge_2={payload}')
        case 'EELNBG014/Cryo/tic/gauges/3':
            st_gauge_3.text(f'gauge_3={payload}')
        case 'EELNBG014/Cryo/log_msg':
            st_status_info.text(f'log_msg={payload}')
        case _:
            # print(f'Received unkown topic: {topic}:{payload}')
            st_status_info.text(f'Received unkown topic: {topic}:{payload}')
            pass
    pass


def on_subscribe(client, userdata, mid, granted_qos):
    """Handle subscribed callback."""
    # print(f'Client subscribed message ID = {mid} with qos = {granted_qos}')
    st_status_info.text(f'Client subscribed message ID = {mid} with qos = {granted_qos}')
    pass


def on_unsubscribe(client, userdata, mid):
    """Handle unsubscribed callback."""
    # print(f'Client unsubscribed message ID = {mid}')
    st_status_info.text(f'Client unsubscribed message ID = {mid}')
    pass


def unsubscribe_all(client):
    """Unsubscribe from all topics."""
    rc, mid = client.unsubscribe('#')
    st_status_info.text(f'Unsubscribing from: # returned rc = {rc} mid = {mid}')
    pass


def main():
    global client
    try:
        # Configure and connecto MQTT client
        client = mqtt.Client()
        client.connected_flag = False
        # client.will_set(client_id, 'Offline', 1, False)
        # bind call back function
        client.on_connect = on_connect
        client.on_disconnect = on_disconnect
        # client.on_log = on_log
        client.on_publish = on_publish
        client.on_message = on_message
        client.on_subscribe = on_subscribe
        client.on_unsubscribe = on_unsubscribe
        result = client.connect(broker, port=1883, keepalive=60, bind_address='')
        print(f'Connecting client {client_id} to MQTT broker: {broker} Result: {result}')
        client.loop_forever()
    except Exception as e:
        print(f'Exception caught! {e}')
        client.connected_flag = False
        unsubscribe_all(client)
        print('Publishing: disconnected')
        rc, mid = client.publish(client_id, 'disconnected')
        print(f'Publishing: disconnected returned rc = {rc} mid = {mid}')
        print(f'Disonnecting from broker: {broker}: {client.disconnect()}')
        # client.disconnect()
        time.sleep(1)
        print('Stopping message loop')


if __name__ == '__main__':
    print('Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
    Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
    eMail: H.Brand@gsi.de\n\
    Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n\
    Published under EUPL.\n')
    main()
    print('Script execution stopped.')
    sys.exit()
