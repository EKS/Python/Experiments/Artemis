#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 12:21:50 2024

@author: brand
"""
import socket
# Variables for the ES IP and Serial Port
# esServer = '140.181.74.100'  # aptdev011
# esPort = 9003
esServer = '140.181.93.181'  # eeldev002
esPort = 8000
try:
    # Create an instance to use the connection
    esSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connect to the ES device using a Raw TCP socket
    esSocket.connect((esServer, esPort))
    # Send a string to the ES device to be sent through the serial port
    esSocket.send(b'*IDN?\r')
    # Receive expected bytes from the buffer
    # Expected response: '\\x13KEITHLEY INSTRUMENTS INC.,MODEL 2700,1089596,B09  /A02  \\x11\\r''
    if False:
        print(esSocket.recv(59))
    else:
        sn = ''
        n = 0
        while True:
            n += 1
            b = esSocket.recv(1)
            # print(f'{n}: {b}')
            match b:
                case b'\x11':
                    pass
                case b'\x13':
                    continue
                case b'\r':
                    break
                case _:
                    sn += b.decode('utf-8')
        print(f'SN: {sn}')
except Exception as e:
    print(e)
finally:
    if esSocket:
        esSocket.close()
