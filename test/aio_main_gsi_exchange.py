#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test sending eMail.

Migrated from: D:\Artemis\Projektvorlage\Application VI\AmbientAll_FoV.vi

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
from email.message import EmailMessage
import aiosmtplib
import datetime

helium_level = 51.0
nitrogen_level = 91.0


async def send_status_mail():
    message = EmailMessage()
    message['From'] = 'H.Brand@gsi.de'
    message['To'] = 'Bi.Reich@gsi.de, A.Krishnan@gsi.de'
    message['Cc'] = 'H.Brand@gsi.de'
    message['Subject'] = 'Testmail:GOOD MORNING ARTEMIS'
    content = 'Dear colleagues\n'
    content += 'This is a test mail. Reported values are not valid.\n\n'
    content += f'Artemis Cryo Filling levels at {datetime.datetime.now()}\n'
    content += f'- Helium level = {helium_level}%\n'
    content += f'- Nitrogen level = {nitrogen_level}%\n'
    content += '\nBest Holger\n'
    message.set_content(content)
    print(f'Sending eMail Message: {message}', flush=True)
    await aiosmtplib.send(message,
                          hostname='email.gsi.de',
                          port=587,
                          username=None,
                          password=None,
                          use_tls=False,
                          start_tls=None
                          )

asyncio.run(send_status_mail())
