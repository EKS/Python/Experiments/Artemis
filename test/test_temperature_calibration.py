#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 13:37:54 2024

@author: H.Brand@gsi.de
"""

import numpy as np
from numpy.polynomial import Polynomial as P

import pint
ureg = pint.UnitRegistry(system='SI')
print(f'Pint using {ureg.default_system} system.')
ureg.default_format = '~P'

# Temperature calibration coefficients to K copied from LabVIEW
# Artemis\Projektvorlage\SubVIs\Keithley2700\MeasureTemperature.vi
ks2700_temperature_calibration_coefficients: list[list[float]] = [
    [+00086.0260, -00931.5300, +04167.0000, -09445.1000, +11570.0000, -07037.1000, +01901.9000],  # sens4
    # [-00008.8227, +00108.1700, -00414.5000, +00976.6200, -01300.2000, +01070.5000, -00165.5200],  # sens5 unused
    [+00060.3290, -00687.9100, +03253.8000, -07699.4000, +09730.2000, -06066.0000, +01656.1000],  # sens6
    [+00035.3780, -00323.7100, +01260.6000, -02366.2000, +02316.1000, -00901.4400, +00282.1700],  # sens7
    [+00029.0850, -00306.3200, +01366.3000, -02936.6000, +03289.7000, -01682.8000, +00397.5300]]  # 664?


# print(f'ks2700_temperature_calibration_coefficients:\n{ks2700_temperature_calibration_coefficients}')

t_calib_coef = np.asarray(ks2700_temperature_calibration_coefficients)
# t_calib_coef = np.delete(np.asarray(ks2700_temperature_calibration_coefficients), 1, 0)
# print(f't_calib_coef:\n{t_calib_coef}')
print(f'Shape of t_calib_coef: {t_calib_coef.shape}')

p_t_calib = [P(t_calib_coef[ii]) for ii in range(t_calib_coef.shape[0])]
# print(f'p_t_calib: {p_t_calib}')

raw_values = [+9.9E37, +1.00475928E+03, -5.49915039E+02, +9.9E37]
# print(f'raw_values: {raw_values}')
t: list[pint.Quantity] = [p_t_calib[ii](1000. / raw_values[ii]) * ureg.K for ii in range(4)]
# print(f't: {t}')
for raw, t in zip(raw_values, t):
    print(f't({raw}) = {t:.3f~#P}')
