#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains classes for communication via serial line and socket.

- AsyncioComInterface: Base class
- AsyncioSerialInterface: Derived class of CommunictionInterface
- AsyncioSocketInterface: Derived class of CommunictionInterface

Known issues:
- Basic function are implemented for now.
- Missing functionalitiy can be added on demand.

Testing
This module can be tested using
- scpi_device_simulator_ethernet.py and/or
- scpi_device_simulator_serial.py
to be found at https://git.gsi.de/EKS/Python/ACDAQ/PyAcdaq/-/tree/master/examples/scpi_device?ref_type=heads

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
from abc import ABC, abstractmethod
import asyncio
import logging
import re
import typing
import aioserial

aci_logger = logging.getLogger('asyncio_com_interface')


class AsyncioComInterface():
    """Base class for device communication via asyncio."""

    def __init__(self, timeout: float = 60):
        self._timeout = timeout

    @abstractmethod
    async def __aenter__(self):
        """Enter context manager."""
        aci_logger.debug('AsyncioComInterface: Entering context manager...')
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def __aexit__(self, exc_type, exc, tb):
        """Exit context manager."""
        aci_logger.debug('AsyncioComInterface: Exiting context manager...')
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def open(self) -> None:
        """Open connection to device."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def read_bytes(self, size: int = 1) -> bytes:
        """Read bytes from device."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def read_line(self) -> str:
        """Read line from device (until NL)."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def read_until(self, termination: str) -> str:
        """Read from device until termination."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def write(self, message: str, termination: str) -> None:
        """Write line with termination to device."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def write_bytes(self, bytes_to_write: bytes) -> None:
        """Write bytes to device."""
        raise NotImplementedError('This class ist abstract.')

    @abstractmethod
    async def close(self) -> None:
        """Close connection to device."""
        raise NotImplementedError('This class ist abstract.')

    @staticmethod
    def create(*args, **kwargs):
        """Create an concrete instance of a class derived from this. (Factory Pattern)."""
        aci_logger.info(f'AsyncioComInterface.create: Parameters: {args}, Keyword-Parameters: {kwargs}')
        # print(kwargs)
        # for key in kwargs:
        #     print(f'{key} = {kwargs[key]}')
        try:
            if 'timeout' in kwargs:
                timeout: int = kwargs["timeout"]
            else:
                timeout = 60
            if 'interface' in kwargs:
                if not kwargs['interface']:
                    raise ValueError('Interface not specified')
                try:
                    host: str = re.split(':', kwargs['interface'])[0]
                    port: int = int(re.split(':', kwargs['interface'])[1])
                    aci_logger.info(f'AsyncioComInterface: Connect to {host}:{port} with timeout of {timeout}s')
                    return AsyncioSocketInterface(timeout=timeout, host=host, port=port)
                except IndexError:
                    interface: str | None = kwargs.get('interface')
                    # Set serial communication parameters
                    baudrate: int = kwargs.get('baudrate', 9600)
                    bytesize: int = kwargs.get('bytesize', aioserial.EIGHTBITS)
                    parity: str = kwargs.get('parity', aioserial.PARITY_NONE)
                    stopbits: int = kwargs.get('stopbits', aioserial.STOPBITS_ONE)
                    xonxoff: bool = kwargs.get('xonxoff', False)
                    aci_logger.info(f'AsyncioComInterface: Connect to {interface} using {baudrate=}, {bytesize=}, {parity=}, {stopbits=}, {xonxoff=} with timeout of {timeout}s')
                    return AsyncioSerialInterface(timeout=timeout, interface=interface, baudrate=baudrate, bytesize=bytesize, parity=parity, stopbits=stopbits, xonxoff=xonxoff)
        except KeyError as ex:
            aci_logger.exception(f'AsyncioComInterface.create: Exception cought. {ex}')
            raise KeyError from ex


class AsyncioSerialInterface(AsyncioComInterface):
    """Derived class for device communication via aioserial."""

    @typing.override
    def __init__(self,
                 timeout: float = 60,
                 interface: str | None = None,
                 baudrate: int = 9600,
                 bytesize=aioserial.EIGHTBITS,
                 parity=aioserial.PARITY_NONE,
                 stopbits=aioserial.STOPBITS_ONE,
                 xonxoff: bool = False):
        super().__init__(timeout)
        self._interface = interface
        self._baudrate = baudrate
        self._bytesize = bytesize
        self._parity = parity
        self._stopbits = stopbits
        self._xonxoff = xonxoff
        self._ser = None

    @typing.override
    async def __aenter__(self):
        """Enter context manager."""
        aci_logger.debug('AsyncioSerialInterface: Entering context manager...')
        await self.open()
        return self

    @typing.override
    async def __aexit__(self, exc_type, exc, tb):
        """Exit context manager."""
        await self.close()
        aci_logger.debug('AsyncioSerialInterface: Exiting context manager...')

    @typing.override
    async def open(self) -> None:
        """Open connection to device."""
        self._ser = aioserial.AioSerial(
            self._interface,
            baudrate=self._baudrate,
            bytesize=self._bytesize,
            parity=self._parity,
            stopbits=self._stopbits,
            xonxoff=self._xonxoff,
            timeout=self._timeout)

    @typing.override
    async def read_bytes(self, size: int = 1) -> bytes:
        """Read bytes from device."""
        assert self._ser
        bytes_read: int = await self._ser.read_async(size)
        aci_logger.debug(f'AsyncioSerialInterface.read_bytes(): Received = {bytes_read}')
        return bytes_read

    @typing.override
    async def read_line(self) -> str:
        """Read line from device (until NL)."""
        assert self._ser
        line = await self._ser.readline_async()
        aci_logger.debug(f'AsyncioSerialInterface.read_line(): Received = {line}')
        return re.split('\n', line.decode('utf-8'))[0]

    @typing.override
    async def read_until(self, termination: str) -> str:
        """Read from device until termination."""
        assert self._ser
        line = await self._ser.read_until_async(termination.encode('utf-8'))
        aci_logger.debug(f'AsyncioSerialInterface.read_until(): Received = {line}')
        return re.split(termination, line.decode('utf-8'))[0]

    @typing.override
    async def write(self, message: str, termination: str) -> None:
        """Write string with termination to device."""
        assert self._ser
        str_to_send = message + termination
        aci_logger.debug(f'AsyncioSerialInterface.write(): Sending: {str_to_send}')
        await self._ser.write_async(str_to_send.encode('utf-8'))

    @typing.override
    async def write_bytes(self, bytes_to_write: bytes) -> None:
        """Write bytes to device."""
        assert self._ser
        aci_logger.debug(f'AsyncioSerialInterface.write(): Sending: {bytes_to_write}')
        await self._ser.write_async(bytes_to_write)

    @typing.override
    async def close(self) -> None:
        """Close connection to device."""
        if self._ser:
            self._ser.close()
            self._ser = None


class AsyncioSocketInterface(AsyncioComInterface):
    """Derived class for device communication via asyncio streams."""

    @typing.override
    def __init__(self, timeout: float = 60, host: str | None = None, port: int | None = None):
        super().__init__(timeout)
        self._host = host
        self._port = port
        # self._timeout = timeout
        self._reader: asyncio.StreamReader | None = None
        self._writer: asyncio.StreamWriter | None = None

    @typing.override
    async def __aenter__(self):
        """Enter context manager."""
        aci_logger.debug('AsyncioSocketInterface: Entering context manager...')
        await self.open()
        return self

    @typing.override
    async def __aexit__(self, exc_type, exc, tb):
        """Exit context manager."""
        await self.close()
        aci_logger.debug('AsyncioSocketInterface: Exiting context manager...')

    @typing.override
    async def open(self) -> None:
        """Open connection to device."""
        self._reader, self._writer = await asyncio.open_connection(self._host, self._port)

    @typing.override
    async def read_bytes(self, size: int = 1) -> bytes:
        """Read bytes from device."""
        assert self._reader
        bytes_read: bytes = await self._reader.read(size)
        aci_logger.debug(f'AsyncioSocketInterface.read_bytes(): Received = {bytes_read!r}')
        return bytes_read

    @typing.override
    async def read_line(self) -> str:
        """Read line from device (until NL)."""
        assert self._reader
        line: bytes = await self._reader.readline()
        aci_logger.debug(f'AsyncioSocketInterface.read_line(): Received: {line!r}')
        return re.split('\n', line.decode('utf-8'))[0]

    @typing.override
    async def read_until(self, termination: str) -> str:
        """Read from device until termination."""
        assert self._reader
        line: bytes = await self._reader.readuntil(termination.encode('utf-8'))
        aci_logger.debug(f'AsyncioSocketInterface.read_until(): Received: {line!r}')
        return re.split(termination, line.decode('utf-8'))[0]

    @typing.override
    async def write(self, message, termination: str) -> None:
        """Write string with termination to device."""
        assert self._writer
        str_to_send = message + termination
        aci_logger.debug(f'AsyncioSocketInterface.write(): Sending: {str_to_send}')
        self._writer.write(str_to_send.encode('utf-8'))
        await self._writer.drain()

    @typing.override
    async def write_bytes(self, bytes_to_write: bytes) -> None:
        """Write bytes to device."""
        assert self._writer
        aci_logger.debug(f'AsyncioSocketInterface.write(): Sending: {bytes_to_write!r}')
        self._writer.write(bytes_to_write)
        await self._writer.drain()

    @typing.override
    async def close(self) -> None:
        """Close connection to device."""
        if self._writer:
            self._writer.close()
            await self._writer.wait_closed()
            self._reader = None
            self._writer = None
