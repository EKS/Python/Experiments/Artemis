#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Definition of Artemis specifc exceptions.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""


class ExitApplication(Exception):
    """
    Derived class of Exception.

    This exception is maybe raised due to user request.
    """

    def __init__(self, message: str):
        self.message = message


if __name__ == '__main__':
    print("exceptions.py\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    # raise ExitApplication('Test ExitApplication exception.')
    # raise KS2700InvalidInputSwitchState('Test KS2700InvalidInputSwitchState exception.')
    print(__name__, 'Done.')
