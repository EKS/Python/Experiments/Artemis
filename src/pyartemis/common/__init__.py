#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains some common stuff for the pyartemis package.

Methods
-------
async def publish_log_msg(mqtt_client, topic, msg)

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
