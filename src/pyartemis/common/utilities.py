#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Monitor Artemis cryo system.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import logging
import aiomqtt

cryo_logger = logging.getLogger("cryo")


async def publish_log_msg(mqtt_client: aiomqtt.Client, topic: str, msg: str):
    """Publish log message to MQTT broker."""
    cryo_logger.debug(f'publish_log_msg: {msg}')
    if mqtt_client:
        await mqtt_client.publish(topic, payload=msg, retain=True)
