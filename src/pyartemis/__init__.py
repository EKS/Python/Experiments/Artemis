#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Artemis contains a Python class library for Automation Control & Data Acquisition System for the
[Artemis experiment](https://www.gsi.de/en/work/research/appamml/atomic_physics/experimental_facilities/hitrap/experiments/artemis).

- Set unit system.
- Create some project specific loggers.

Methods
-------

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import logging
import pint

ureg = pint.UnitRegistry()
ureg.default_format = "~P"

logging.getLogger('cryo').addHandler(logging.NullHandler())
logging.getLogger('cryo.level').addHandler(logging.NullHandler())
logging.getLogger('cryo.pressure').addHandler(logging.NullHandler())
logging.getLogger('cryo.temperature').addHandler(logging.NullHandler())
logging.getLogger('asyncio_com_interface').addHandler(logging.NullHandler())
