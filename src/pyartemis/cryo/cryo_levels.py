#!/usr/bin/env python3
# -*- coding: utf-8 -*-
r"""
Read some parameters from cryogenic monitor E5025.

Migrated from: D:\\Artemis\\Projektvorlage\\Application VI\\AmbientAll_FoV.vi

Known issues:
- E5025 response does not contain valid values for nitrogen level.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
import datetime
import inspect
import logging
import re
import typing
import aioserial  # type: ignore
import aiomqtt
from pyartemis.cryo import cryo_globals
# from pyartemis.cryo.cryo_globals import ureg
from pyartemis.common import asyncio_com_interface
from pyartemis.common import utilities
import pyartemis.main_cryo as cryo

cryo_e5025_logger = logging.getLogger("cryo.level")


async def e5025_transaction(mqtt_client: aiomqtt.Client,
                            com_lock: asyncio.Lock,
                            com_interface: asyncio_com_interface.AsyncioComInterface,
                            command: str
                            ) -> str:
    """
    Perform low , com_lock=e5025_com_locklevel write-read communication with device E5025.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in e5025_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface
        Interface connection to E5025 device, serial or socket.
    command : str
        E5025 command string.

    Returns
    -------
    response : strks2700_task:
        Response received from E5025.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(command, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid command.')
    if len(command) == 0:
        raise ValueError('f{tn}.{cn}.{fn}: Invalid command.')
    lf: typing.Final[str] = '\n'
    async with com_lock:
        while True:
            try:
                cryo_e5025_logger.debug(f'{tn}.{cn}.{fn}: {command=}')
                write_task: asyncio.Task = asyncio.create_task(com_interface.write(command, lf), name=f'{tn}.{cn}.{fn}.write')
                read_task: asyncio.Task = asyncio.create_task(com_interface.read_until(lf), name=f'{tn}.{cn}.{fn}.read')
                try:
                    async with asyncio.timeout(10):
                        await write_task
                        response: str = await read_task
                        cryo_e5025_logger.debug(f'{tn}.{cn}.{fn}: {command=} -> {response=}')
                        return response
                except asyncio.TimeoutError as ex:
                    cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: Timeout exception cought. {ex}')
            except UnicodeDecodeError as ex:
                cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: UnicodeDecodeError cought. {ex}')


async def e5025_readout_loop(mqtt_client: aiomqtt.Client,
                             wakeup_event: asyncio.Event,
                             com_lock: asyncio.Lock,
                             com_interface: asyncio_com_interface.AsyncioComInterface,
                             smtp_queue: asyncio.Queue | None = None
                             ) -> typing.NoReturn:
    """
    Perform periodic readout of device E5025. Publish status to MQTT broker and send_status_mail task.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    wakeup_event : asyncio.Event, optional
        Event used to wakeup periodic loop if e5025_readout_interval was changed by MQTT. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in e5025_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface, optional
        Interface connection to E5025 device, serial or socket. The default is None.
    smtp_queue : asyncio.Queue, optional
        Queue to send status information to cryo.send_status_mail(). The default is None.

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(wakeup_event, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid wakeup_event.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError('f{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(smtp_queue, asyncio.Queue | None):
        raise TypeError('f{tn}.{cn}.{fn}: Invalid smtp_queue.')
    cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: Using {com_interface}')
    try:
        await mqtt_client.publish(cryo_globals.topic_prefix + '/e5025/idn', payload='E5025', retain=True)
    except Exception:
        pass
    e5025_iteration_counter: int = 0
    while True:
        e5025_iteration_counter += 1
        response: str = await e5025_transaction(mqtt_client, com_lock, com_interface, command='W')
        # response: str = 'E5025,Hp,P1D0914m+058%,P2D####m+###%,HL0580,HT004,HC110,PR121,PL1165,HMN,ND085,N2Y03945,NF03850,NE04571,SA128,SB068,t 16 08 05 09 00,W*'  # Expeted instrument response.
        if not response:
            await asyncio.sleep(1)
            continue
        e5025_readout_time: datetime.datetime = datetime.datetime.now()
        items: list[str] = re.split(',', response)
        if len(items) < 18:  # Repeat query if response is not complete
            cryo_e5025_logger.warning(f'{tn}.{cn}.{fn}: Response is incomplete. {response=}')
            continue
        if not re.fullmatch('E5025', items[0]):
            continue
        try:
            level_liquid_helium: float = (float(re.split('%', re.split('m', items[2])[1])[0])) / 100.
        except (IndexError, TypeError) as e:
            cryo_e5025_logger.warning(f'{tn}.{cn}.{fn}: IndexError or TypeError: {e}')
            level_liquid_helium = float('nan')
        try:
            n2y: float = float(items[11][3:])
            nf: float = float(items[12][2:])
            ne: float = float(items[13][2:])
            level_liquid_nitrogen: float = ((n2y - ne) / ((nf - ne) / 100.)) / 100.
        except (IndexError, TypeError) as e:
            cryo_e5025_logger.warning(f'{tn}.{cn}.{fn}: IndexError or TypeError: {e}')
            level_liquid_nitrogen = float('nan')
        cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: {e5025_readout_time} {level_liquid_helium=:.3f}, {level_liquid_nitrogen=:.3f}')
        if smtp_queue:
            info: dict[str, str | dict[str, datetime.datetime | float]] = {
                'task': 'e5025',
                'status': {
                    'readout_time': e5025_readout_time,
                    'helium_level': level_liquid_helium,
                    'nitrogen_level': level_liquid_nitrogen}}
            await smtp_queue.put(info)
        try:
            await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{e5025_readout_time} Levels: helium={level_liquid_helium:.3f}, nitrogen={level_liquid_nitrogen:.3f}')
            await mqtt_client.publish(cryo_globals.topic_prefix + '/e5025/iteration_counter', payload=e5025_iteration_counter)
            await mqtt_client.publish(cryo_globals.topic_prefix + '/e5025/readout_time', payload=str(e5025_readout_time), retain=True)
            await mqtt_client.publish(cryo_globals.topic_prefix + '/level_liquid_helium', level_liquid_helium, retain=True)
            await mqtt_client.publish(cryo_globals.topic_prefix + '/level_liquid_nitrogen', level_liquid_nitrogen, retain=True)
        except Exception:
            pass
        await cryo.event_wait(wakeup_event, cryo_globals.e5025_readout_interval)


async def e5025_subscribe_set_topics(mqtt_client: aiomqtt.Client) -> str:
    """
    Subscribe to MQTT topics belonging to devices e5025.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    command_queue : asyncio.Queue
        Queue to send command from console to e5025 command handler.

    Returns
    -------
    subtopic: str.
        Subtopic to be used in match by caller.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    cryo_e5025_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    try:
        # Publish topics with default values
        await mqtt_client.publish(cryo_globals.topic_prefix + '/e5025/readout_interval', payload=cryo_globals.e5025_readout_interval.to_base_units().magnitude, retain=True)
        await mqtt_client.publish(cryo_globals.topic_prefix + '/e5025/set_readout_interval', payload=cryo_globals.e5025_readout_interval.to_base_units().magnitude, retain=False)
        # Subscribe to desired topics
        await mqtt_client.subscribe(cryo_globals.topic_prefix + '/e5025/set_readout_interval')
        return 'e5025'
    except Exception as ex:
        cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: Exception cought trying to publish readout_intervals: {ex}')


async def e5025_handle_set_topics(mqtt_client: aiomqtt.Client,
                                  wakeup_event: asyncio.Event,
                                  cmd_queue: asyncio.Queue,
                                  subtopic: str,
                                  payload: str
                                  ) -> None:
    """
    Handle MQTT topics belonging to devices e5025.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    wakeup_event : asyncio.Event
        Event to wakeup e5025_readout_loop.
    command_queue : asyncio.Queue
        Queue to send command from console to e5025 command handler.
    subtopic : str
        Subtopic to be handled.

    Returns
    -------
    None

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(wakeup_event, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid wakeup_event.')
    if not isinstance(cmd_queue, asyncio.Queue):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid cmd_queue.')
    if not isinstance(subtopic, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid subtopic.')
    if not isinstance(payload, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid payload.')
    cryo_e5025_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: {subtopic}={payload}')
    try:
        match subtopic:
            case 'set_readout_interval':
                try:
                    cryo_globals.e5025_readout_interval = float(payload) * cryo_globals.ureg.s
                    cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: e5025_readout_interval={cryo_globals.e5025_readout_interval}')
                    await mqtt_client.publish(cryo_globals.topic_prefix + '/e5025/readout_interval', payload=cryo_globals.e5025_readout_interval.to_base_units().magnitude, retain=True)
                    wakeup_event.set()
                except Exception as ex:
                    cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: Exception during conversion of e5025_readout_interval. ({payload!r}). {ex}')
                    await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'Exception during conversion of e5025_readout_interval. ({payload!r})')
            case _:
                pass
    except Exception as ex:
        cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: Exception cought trying to handle {subtopic}={payload!r} {ex}')


def e5025_command_help() -> str:
    """Return help text for command line interface."""
    help_text: str = 'e5025: No commands available.\n'
    return help_text


async def e5025_command_loop(mqtt_client: aiomqtt.Client,
                             com_lock: asyncio.Lock,
                             com_interface: asyncio_com_interface.AsyncioComInterface,
                             cmd_queue: asyncio.Queue
                             ) -> typing.NoReturn:
    """
    Handle commands received from queue.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in e5025_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface, optional
        Interface connection to TIC6G device, serial or socket. The default is None.
    cmd_queue : asyncio.Queue, optional
        Queue receiving command from console. The default is None.

    Returns
    -------
    None

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not mqtt_client:
        raise ValueError(f'{tn}.{cn}.{fn}: mqtt_client not specified.')
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(cmd_queue, asyncio.Queue):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid cmd_queue.')
    try:
        while True:
            cryo_e5025_logger.debug('{tn}.{cn}.{fn}: wait for queue.')
            q_item: list[str] = await cmd_queue.get()
            try:
                match q_item[0]:
                    case _:
                        cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: Invalid command received. {q_item}')
                        print(f'{tn}.{cn}.{fn}: Invalid command received. {q_item}')
            except IndexError:
                cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: IndexError cought.')
    except Exception as ex:
        cryo_e5025_logger.exception('f{tn}.{cn}.{fn}: Exception cought.')
        raise Exception(f'{tn}.{cn}.{fn}: Exception cought.') from ex


async def e5025_task(mqtt_client: aiomqtt.Client,
                     wakeup_event: asyncio.Event,
                     com_lock: asyncio.Lock,
                     interface: str,
                     smtp_queue: asyncio.Queue | None = None
                     ) -> typing.NoReturn:
    """
    Task to run coroutines belonging device E5025.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    wakeup_event : asyncio.Event, optional
        Event used to wakeup periodic loop if e5025_readout_interval was changed by MQTT. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in e5025_transaction. The default is None.
    interface : str, optional
        Interface to E5025 device, serial or socket, eg. com3, /dev/ttyUSB0, host:port. The default is None.
    smtp_queue : asyncio.Queue, optional
        Queue to send status information to cryo.send_status_mail(). The default is None.com_lock

    Returnsresponse
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(wakeup_event, asyncio.Event):
        raise TypeError('f{tn}.{cn}.{fn}: Invalid wakeup_event.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(interface, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid interface.')
    if not isinstance(smtp_queue, asyncio.Queue | None):
        raise TypeError('f{tn}.{cn}.{fn}: Invalid smtp_queue.')
    cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: Connection to E5025 at {interface=}')
    connection_parameter: dict[str, bool | int | str] = {
        'timeout': 10,
        'interface': interface,
        'baudrate': 9600,
        'bytesize': aioserial.EIGHTBITS,
        'parity': aioserial.PARITY_NONE,
        'stopbits': aioserial.STOPBITS_ONE,
        'xonxoff': True}
    try:
        cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: Using AsyncioComInterface context.')
        async with asyncio_com_interface.AsyncioComInterface.create(**connection_parameter) as com_interface:
            cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: Using AsyncioComInterface connection established.')
            await e5025_readout_loop(mqtt_client, wakeup_event, com_lock, com_interface, smtp_queue=smtp_queue)
    except asyncio.CancelledError as ex:
        cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception. {ex}')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception. {ex}')
        raise asyncio.CancelledError(f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception.') from ex
    except ConnectionResetError as ex:
        cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception. {ex}')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception. {ex}')
        raise ConnectionResetError(f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception.') from ex
    except aioserial.SerialException as ex:
        cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception. {ex}')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception. {ex}')
        raise aioserial.SerialException(f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception.') from ex
    except Exception as ex:
        cryo_e5025_logger.exception(f'{tn}.{cn}.{fn}: Exception cought. Re-raising exception. {ex}')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: Exception cought. Re-raising exception. {ex}')
        raise Exception(f'{tn}.{cn}.{fn}: Execption cought. Re-raising exception.') from ex
    finally:
        cryo_e5025_logger.info(f'{tn}.{cn}.{fn}: Done.')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: Done.')
