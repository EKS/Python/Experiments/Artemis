#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Definition of cryo global variables.

Migrated from: D:\\Artemis\\Projektvorlage\\Application VI\\AmbientAll_FoV.vi

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import typing
import pint

ureg = pint.UnitRegistry(system='SI')
ureg.default_format = "~P"

topic_prefix: str = 'Artemis/Cryo'
topic_log_msg: str = topic_prefix + '/log_msg'

E5025_MIN_READOUT_INTERVAL: typing.Final[pint.Quantity] = 60. * ureg.s  # Recommended: 1800s for monitoring and 60s during filling.
KS2700_MIN_READOUT_INTERVAL: typing.Final[pint.Quantity] = 60. * ureg.s  # Recommended: 1800s for monitoring and 60s during filling.
TIC_MIN_READOUT_INTERVAL: typing.Final[pint.Quantity] = 60. * ureg.s  # Recommended: 600s for monitoring and 60s during filling.

e5025_readout_interval: pint.Quantity = 1800. * ureg.s  # Recommended: 1800s for monitoring and 60s during filling.
ks2700_readout_interval: pint.Quantity = 600. * ureg.s  # Recommended: 600s for monitoring and 60s during filling.
tic_readout_interval: pint.Quantity = 600. * ureg.s  # Recommended: 600s for monitoring and 60s during filling.


# Temperature calibration coefficients to K copied from LabVIEW
# Artemis\Projektvorlage\SubVIs\Keithley2700\MeasureTemperature.vi
KS2700_TEMPERATURE_CALIBRATION_COEFFICIENTS: typing.Final[list[list[float]]] = [
    [+00086.0260, -00931.5300, +04167.0000, -09445.1000, +11570.0000, -07037.1000, +01901.9000],  # sens4
    # [-00008.8227, +00108.1700, -00414.5000, +00976.6200, -01300.2000, +01070.5000, -00165.5200],  # sens5 unused
    [+00060.3290, -00687.9100, +03253.8000, -07699.4000, +09730.2000, -06066.0000, +01656.1000],  # sens6
    [+00035.3780, -00323.7100, +01260.6000, -02366.2000, +02316.1000, -00901.4400, +00282.1700],  # sens7
    [+00029.0850, -00306.3200, +01366.3000, -02936.6000, +03289.7000, -01682.8000, +00397.5300]]  # 664?
