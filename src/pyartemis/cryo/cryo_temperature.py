#!/usr/bin/env python3
# -*- coding: utf-8 -*-
r"""
Read cryo temperature using KEITHLEY 2700.

Migrated from: D:\Artemis\Projektvorlage\Application VI\AmbientAll_FoV.vi

Known issues:
- None

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
import datetime
import inspect
import json
import logging
import re
import typing
import numpy as np
from numpy.polynomial import Polynomial as P
import pint
import aioserial
import aiomqtt
from pyartemis.cryo import cryo_globals
from pyartemis.cryo.cryo_globals import ureg
from pyartemis.common import asyncio_com_interface
from pyartemis.common import utilities
import pyartemis.main_cryo as cryo

cryo_ks2700_logger = logging.getLogger("cryo.temperature")


class KS2700InvalidInputSwitchStateError(Exception):
    """Exception raised by KS2700."""

    def __init__(self, message: str):
        self.message = message


async def ks2700_transaction(mqtt_client: aiomqtt.Client,
                             com_lock: asyncio.Lock,
                             com_interface: asyncio_com_interface.AsyncioComInterface,
                             command: str) -> str:
    """
    Perform low level write-read communication with device KS2700.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in ks2700_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface
        Interface connection to KS2700 device, serial or socket.
    command : str
        KS2700 command string.

    Returns
    -------
    response : str
        Responce received from KS2700.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(command, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid command.')
    if len(command) == 0:
        raise ValueError('f{tn}.{cn}.{fn}: Invalid command.')
    cr: typing.Final[str] = '\r'  # Refer to device setting: '\n' or '\n' or '\r\n'
    async with com_lock:
        while True:
            try:
                cryo_ks2700_logger.debug(f'{tn}.{cn}.{fn}: {command=}')
                write_task: asyncio.Task = asyncio.create_task(com_interface.write(command, cr), name=f'{tn}.{cn}.{fn}.write')
                read_task: asyncio.Task = asyncio.create_task(com_interface.read_until(cr), name=f'{tn}.{cn}.{fn}.read')
                try:
                    async with asyncio.timeout(10):
                        await write_task
                        if re.search(r'\?', command):
                            response: str = read_task
                            cryo_ks2700_logger.debug(f'{tn}.{cn}.{fn}: {response=}')
                            if response[0] == '\x13':
                                response = response[1:-1]
                            cryo_ks2700_logger.debug(f'{tn}.{cn}.{fn}: {command=} -> {response=}')
                            return response
                except asyncio.TimeoutError as ex:
                    cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: Timeout exception cought. {ex}')
            except UnicodeDecodeError as ex:
                cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: UnicodeDecodeError cought. {ex}')


async def ks2700_configuration(mqtt_client: aiomqtt.Client,
                               com_lock: asyncio.Lock,
                               com_interface: asyncio_com_interface.AsyncioComInterface,
                               scan_list: str = '101:104',
                               sample_count: int = 4,
                               ks2700_front: bool = False) -> None:
    """
    Perform configuration of device KS2700.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in ks2700_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface, optional
        Interface connection to KS2700 device, serial or socket. The default is None.
    scan_list : str, optional
        List of channels to scan. Refer to manual for details. The default is '101:104'.
    sample_count : int, optional
        Number of samples to read. The default is 4.
    ks2700_front : bool, optional
        Sensors are connected to False:front panel or True:rear pnel. The default is False.

    Raises
    ------
    exceptions.KS2700InvalidInputSwitchStateError
        In case of invalid input switch state.

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(scan_list, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid scan_list.')
    if not isinstance(sample_count, int):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid sample_count.')
    if not isinstance(ks2700_front, bool):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid ks2700_front.')
    if len(scan_list) == 0:
        raise ValueError(f'{tn}.{cn}.{fn}: Invalid scan_list.')
    _ = await ks2700_transaction(mqtt_client, com_lock, com_interface, 'SYST:TST:TYPE RTCL')  # Configure timestamp
    _ = await ks2700_transaction(mqtt_client, com_lock, com_interface, 'TRAC:CLE')  # Clear data buffer
    response: str = await ks2700_transaction(mqtt_client, com_lock, com_interface, 'SYST:FRSW?')  # Validate input switch state
    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: Validate input switch state: {response=}')
    if ks2700_front and int(response[0]) == 1:  # Sensor connection 0:front; 1:rear.
        pass
    elif not ks2700_front and int(response[0]) == 0:  # Sensor connection 0:front; 1:rear.
        pass
    else:
        raise KS2700InvalidInputSwitchStateError('f{tn}.{cn}.{fn}: Check sensor connection switch on front panel.')
    # configure_measurement_command = f':FUNC "FRES" , (@101:104)\n;:FRES:RANG:AUTO OFF , (@101:104)\n;:FRES:RANG 4000.000000 , (@101:104)\n;:FRES:DIG 7 , (@101:104)'
    configure_measurement_command: str = f':FUNC "FRES" , (@{scan_list})\n;:FRES:RANG:AUTO OFF , (@{scan_list})\n;:FRES:RANG 4000.000000 , (@{scan_list})\n;:FRES:DIG 7 , (@{scan_list})'
    _ = await ks2700_transaction(mqtt_client, com_lock, com_interface, configure_measurement_command)  # Configure measurement
    configure_scan_command: str = f'ROUT:SCAN (@{scan_list})\n;ROUT:SCAN:LSEL INT\n;SAMP:COUN {sample_count}'
    _ = await ks2700_transaction(mqtt_client, com_lock, com_interface, configure_scan_command)  # Configure scan


async def ks2700_readout_loop(mqtt_client: aiomqtt.Client,
                              wakeup_event: asyncio.Event,
                              com_lock: asyncio.Lock,
                              com_interface: asyncio_com_interface.AsyncioComInterface,
                              smtp_queue: asyncio.Queue | None = None,
                              ks2700_front: bool = False) -> typing.NoReturn:
    """
    Perform periodic readout of device KS2700. Publish status to MQTT broker and send_status_mail task.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    wakeup_event : asyncio.Event, optional
        Event used to wakeup periodic loop if ks2700_readout_interval was changed by MQTT. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in ks2700_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface, optional
        Interface connection to KS2700 device, serial or socket. The default is None.
    smtp_queue : asyncio.Queue, optional, xl_lock= xl_lock
        Queue to send status information to cryo.send_status_mail(). The default is None.
    ks2700_front : bool, optional
        Sensors are connected to False:front panel or True:rear pnel. The default is False.

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(smtp_queue, asyncio.Queue | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid smtp_queue.')
    if not isinstance(ks2700_front, bool):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid ks2700_front.')
    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: Using {com_interface=}')
    t_calib_coef: np.ndarray = np.asarray(cryo_globals.KS2700_TEMPERATURE_CALIBRATION_COEFFICIENTS)
    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: {t_calib_coef.shape=}')
    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: {t_calib_coef=}')
    p_t_calib: list[P] = [P(t_calib_coef[ii]) for ii in range(t_calib_coef.shape[0])]
    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: p_t_calib: {p_t_calib}')
    idn: str = await ks2700_transaction(mqtt_client, com_lock, com_interface, '*IDN?')
    try:
        await mqtt_client.publish(cryo_globals.topic_prefix + '/ks2700/idn', payload=idn, retain=True)
    except Exception:
        pass
    await ks2700_configuration(mqtt_client, com_lock, com_interface, scan_list='101:104', sample_count=4, ks2700_front=ks2700_front)
    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: {idn=}')
    ks2700_iteration_counter: int = 0
    while True:  # Periodic readout
        ks2700_iteration_counter += 1
        read_multiple_direct_command: str = 'FORM:ELEM READ,CHAN,TST,RNUM\n;READ?'
        response: str = await ks2700_transaction(mqtt_client, com_lock, com_interface, read_multiple_direct_command)
        # Simulate instrument response.
        # response = '+9.32568264E+00,13:27:49.52, 7-Feb-2024,+00000,000,+9.32568264E+00,13:27:49.63, 7-Feb-2024,+00001,000,+9.32568264E+00,13:27:49.73,7-Feb-2024,+00002,000,+9.32568359E+00,13:27:49.84, 7-Feb-2024,+00003,000'
        if not response:
            await asyncio.sleep(1)
            continue
        ks2700_readout_time: datetime.datetime = datetime.datetime.now()
        cryo_ks2700_logger.debug(f'{tn}.{cn}.{fn}: {ks2700_readout_time} Scan={response}')
        try:
            raw_values: list[float] = [float(raw_str) for raw_str in response.split(',')[::5]]
        except ValueError as ex:
            cryo_ks2700_logger.warning(f'{tn}.{cn}.{fn}: Unexpected response. {ex}')
            continue
        if ks2700_front:  # Extend when using one channel connected to ks2700_front.
            raw_values.append(raw_values[0])
            raw_values.append(raw_values[0])
            raw_values.append(raw_values[0])
        cryo_ks2700_logger.debug(f'{tn}.{cn}.{fn}: {raw_values=}')
        temperatures: list[pint.Quantity] = [p_t_calib[ii](1000. / raw_values[ii]) * ureg.K for ii in range(t_calib_coef.shape[0])]
        cryo_ks2700_logger.info(f'{ks2700_readout_time} {temperatures=}')
        if smtp_queue:
            info: dict[str, str | dict[str, datetime.datetime | list[pint.Quantity]]] = {
                'task': 'ks2700',
                'status': {
                    'readout_time': ks2700_readout_time,
                    'temperatures': temperatures}}
            await smtp_queue.put(info)
        try:
            await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{ks2700_readout_time} Temperatures={response}')
            await mqtt_client.publish(cryo_globals.topic_prefix + '/ks2700/iteration_counter', payload=ks2700_iteration_counter)
            await mqtt_client.publish(cryo_globals.topic_prefix + '/ks2700/readout_time', payload=str(ks2700_readout_time), retain=True)
            t_magnitudes: list[float] = []
            for ii, temperature in enumerate(temperatures):
                t_magnitude: float = temperature.to_base_units().magnitude
                t_magnitudes.append(t_magnitude)
                await mqtt_client.publish(cryo_globals.topic_prefix + '/temperatures/' + str(ii), payload=t_magnitude, retain=True)
            await mqtt_client.publish(cryo_globals.topic_prefix + '/temperatures', payload=json.dumps(t_magnitudes), retain=True)
            cryo_ks2700_logger.info(f'{ks2700_readout_time} {temperatures=}')
        except Exception:
            pass
        await cryo.event_wait(wakeup_event, cryo_globals.ks2700_readout_interval)


async def ks2700_subscribe_set_topics(mqtt_client: aiomqtt.Client) -> str:
    """
    Subscribe to MQTT topics belonging to devices ks2700.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    command_queue : asyncio.Queue
        Queue to send command from console to ks2700 command handler.

    Returns
    -------
    subtopic: str.
        Subtopic to be used in match by caller.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    cryo_ks2700_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    try:
        # Publish topics with default values
        await mqtt_client.publish(cryo_globals.topic_prefix + '/ks2700/readout_interval', payload=cryo_globals.ks2700_readout_interval.to_base_units().magnitude, retain=True)
        await mqtt_client.publish(cryo_globals.topic_prefix + '/ks2700/set_readout_interval', payload=cryo_globals.ks2700_readout_interval.to_base_units().magnitude, retain=False)
        # Subscribe to desired topics
        await mqtt_client.subscribe(cryo_globals.topic_prefix + '/ks2700/set_readout_interval')
        return 'ks2700'
    except Exception as ex:
        cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: Exception cought trying to publish readout_intervals: {ex}')


async def ks2700_handle_set_topics(mqtt_client: aiomqtt.Client,
                                   wakeup_event: asyncio.Event,
                                   cmd_queue: asyncio.Queue,
                                   subtopic: str,
                                   payload: str
                                   ) -> None:
    """
    Handle MQTT topics belonging to devices ks2700.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    wakeup_event : asyncio.Event
        Event to wakeup ks2700_readout_loop.
    command_queue : asyncio.Queue
        Queue to send command from console to ks2700 command handler.
    subtopic : str
        Subtopic to be handled.

    Returns
    -------
    None

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(wakeup_event, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid wakeup_event.')
    if not isinstance(cmd_queue, asyncio.Queue):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid cmd_queue.')
    if not isinstance(subtopic, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid subtopic.')
    if not isinstance(payload, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid payload.')
    cryo_ks2700_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: {subtopic}={payload}')
    try:
        match subtopic:
            case 'set_readout_interval':
                try:
                    cryo_globals.ks2700_readout_interval = float(payload) * cryo_globals.ureg.s
                    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: ks2700_readout_interval={cryo_globals.ks2700_readout_interval}')
                    await mqtt_client.publish(cryo_globals.topic_prefix + '/ks2700/readout_interval', payload=cryo_globals.ks2700_readout_interval.to_base_units().magnitude, retain=True)
                    wakeup_event.set()
                except Exception as ex:
                    cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: Exception during conversion of ks2700_readout_interval. ({payload!r}). {ex}')
                    await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'Exception during conversion of ks2700_readout_interval. ({payload!r})')
            case _:
                pass
    except Exception as ex:
        cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: Exception cought trying to handle {subtopic}={payload!r} {ex}')


def ks2700_command_help() -> str:
    """Return help text for command line interface."""
    help_text: str = 'ks2700: No commands available.\n'
    return help_text


async def ks2700_command_loop(mqtt_client: aiomqtt.Client,
                              com_lock: asyncio.Lock,
                              com_interface: asyncio_com_interface.AsyncioComInterface,
                              cmd_queue: asyncio.Queue
                              ) -> typing.NoReturn:
    """
    Handle commands received from queue.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in ks2700_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface, optional
        Interface connection to TIC6G device, serial or socket. The default is None.
    cmd_queue : asyncio.Queue, optional
        Queue receiving command from console. The default is None.

    Returns
    -------
    None

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not mqtt_client:
        raise ValueError(f'{tn}.{cn}.{fn}: mqtt_client not specified.')
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(cmd_queue, asyncio.Queue):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid cmd_queue.')
    try:
        while True:
            cryo_ks2700_logger.debug('{tn}.{cn}.{fn}: wait for queue.')
            q_item: list[str] = await cmd_queue.get()
            try:
                match q_item[0]:
                    case _:
                        cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: Invalid command received. {q_item}')
                        print(f'{tn}.{cn}.{fn}: Invalid command received. {q_item}')
            except IndexError:
                cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: IndexError cought.')
    except Exception as ex:
        cryo_ks2700_logger.exception('f{tn}.{cn}.{fn}: Exception cought.')
        raise Exception(f'{tn}.{cn}.{fn}: Exception cought.') from ex


async def ks2700_task(mqtt_client: aiomqtt.Client,
                      wakeup_event: asyncio.Event,
                      com_lock: asyncio.Lock,
                      interface: str,
                      smtp_queue: asyncio.Queue | None = None,
                      ks2700_front: bool = False) -> typing.NoReturn:
    """
    Task to run coroutines belonging device KS2700.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    wakeup_event : asyncio.Event, optional
        Event used to wakeup periodic loop if ks2700_readout_interval was changed by MQTT. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in ks2700_transaction. The default is None.
    interface : str, optional
        Interface to KS2700 device, serial or socket, eg. com3, /dev/ttyUSB0, host:port. The default is None.
    smtp_queue : asyncio.Queue, optional
        Queue to send status information to cryo.send_status_mail(). The default is None.

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(wakeup_event, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid wakeup_event.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(interface, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(smtp_queue, asyncio.Queue | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid smtp_queue.')
    if not isinstance(ks2700_front, bool):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid ks2700_front.')
    cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: Connection to KS2700 at {interface=}')
    connection_parameter: dict[str, bool | int | str] = {
        'timeout': 10,
        'interface': interface,
        'baudrate': 9600,
        'bytesize': aioserial.EIGHTBITS,
        'parity': aioserial.PARITY_NONE,
        'stopbits': aioserial.STOPBITS_ONE,
        'xonxoff': True}
    try:
        async with asyncio_com_interface.AsyncioComInterface.create(**connection_parameter) as com_interface:
            cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: Using AsyncioComInterface connection established.')
            await ks2700_readout_loop(mqtt_client, wakeup_event, com_lock, com_interface, smtp_queue=smtp_queue, ks2700_front=ks2700_front)
    except asyncio.CancelledError as ex:
        cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception. {ex}')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception. {ex}')
        raise asyncio.CancelledError(f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception.') from ex
    except ConnectionResetError as ex:
        cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception. {ex}')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception. {ex}')
        raise ConnectionResetError(f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception.') from ex
    except aioserial.SerialException as ex:
        cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception. {ex}')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception. {ex}')
        raise aioserial.SerialException(f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception.') from ex
    except Exception as ex:
        cryo_ks2700_logger.exception(f'{tn}.{cn}.{fn}: Exception cought. Re-raising exception. {ex}')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: Exception cought. Re-raising exception. {ex}')
        raise Exception(f'{tn}.{cn}.{fn}: Execption cought. Re-raising exception.') from ex
    finally:
        cryo_ks2700_logger.info(f'{tn}.{cn}.{fn}: Done.')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: Done.')
