#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains Artemis cryo stuff.

Methods
-------
async def e5025_task(mqtt_client=None, serial_port=None)
async def ks2700_task(mqtt_client=None, serial_port=None)
async def tic_task(mqtt_client=None, serial_port=None)

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
