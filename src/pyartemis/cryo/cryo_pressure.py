#!/usr/bin/env python3
# -*- coding: utf-8 -*-
r"""
Read cryo temperature using TIC Instrument Controller 6-Gauge.

Migrated from: D:\\Artemis\\Projektvorlage\\Application VI\\AmbientAll_FoV.vi

Known issues: None

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
import datetime
import inspect
import json
import logging
import re
import typing
import pint
import aioserial
import aiomqtt
from pyartemis.cryo import cryo_globals
from pyartemis.cryo.cryo_globals import ureg
from pyartemis.common import asyncio_com_interface
from pyartemis.common import utilities
import pyartemis.main_cryo as cryo

cryo_tic_logger = logging.getLogger("cryo.pressure")

tic_object: dict[str, int] = {
    'Node': 901,
    'TIC Status': 902,
    'Turbo Pump': 904,
    'Turbo speed': 905,
    'Turbo power': 906,
    'Turbo normal': 907,
    'Turbo satandby': 908,
    'Turbo cycle time': 909,
    'Backing Pump': 910,
    'Backing speed': 911,
    'Backing power': 912,
    'Gauge 1': 913,
    'Gauge 2': 914,
    'Gauge 3': 915,
    'Relay 1': 916,
    'Relay 2': 917,
    'Relay 3': 918,
    'PS Temperature': 919,
    'Internal Temperature': 920,
    'Analogue out': 921,
    'Externalvalve': 922,
    'Heater band': 923,
    'ExternalCooler': 924,
    'Display contrast': 925,
    'Configuration Operations': 926,
    'Lock': 928,
    'Pressure Units': 929,
    'PC comms': 930,
    'Default screen': 931,
    'Fixed/Float ASG': 932,
    'System (TIC and TC only)': 933,
    'Gauge 4': 934,
    'Gauge 5': 935,
    'Gauge 6': 936,
    'Relay 4': 937,
    'Relay 5': 938,
    'Relay 6': 939,
    'Gauge Values': 940}
tic_object_by_id: dict[int, str] = dict(zip(tic_object.values(), tic_object.keys()))
tic_response_code: dict[int, str] = {
    0: 'No error',
    1: 'Invalid command for object ID',
    2: 'Invalid query/command',
    3: 'Missing parameter',
    4: 'Parameter out of range',
    5: 'Invalid command in current state - e.g. serial command to start or stop when in parallel control mode',
    6: 'Data checksum error',
    7: 'EEPROM read or write error',
    8: 'Operation took too long',
    9: 'Invalid config ID'}
tic_priority: dict[int, str] = {
    0: 'OK',
    1: 'Warning',
    2: 'Alarm',
    3: 'Alarm'}
tic_alert_id: dict[int, str] = {
    0: 'No Alert',
    1: 'ADC Fault',
    2: 'ADC Not Ready',
    3: 'Over Range',
    4: 'Under Range',
    5: 'ADC Invalid',
    6: 'No Gauge',
    7: 'Unknown',
    8: 'Not Supported',
    9: 'New ID',
    10: 'Over Range',
    11: 'Under Range',
    12: 'Over Range',
    13: 'Ion Em Timeout',
    14: 'Not Struck',
    15: 'Filament Fail',
    16: 'Mag Fail',
    17: 'Striker Fail',
    18: 'Not Struck',
    19: 'Filament Fail',
    20: 'Cal Error',
    21: 'Initialising',
    22: 'Emission Error',
    23: 'Over Pressure',
    24: 'ASG Cant Zero',
    25: 'RampUp Timeout',
    26: 'Droop Timeout',
    27: 'Run Hours High',
    28: 'SC Interlock',
    29: 'ID Volts Error',
    30: 'Serial ID Fail',
    31: 'Upload Active',
    32: 'DX Fault',
    33: 'Temp Alert',
    34: 'SYSI Inhibit',
    35: 'Ext Inhibit',
    36: 'Temp Inhibit',
    37: 'No Reading',
    38: 'No Message',
    39: 'NOV Failure',
    40: 'Upload Timeout',
    41: 'Download Failed',
    42: 'No Tube',
    43: 'Use Gauges 4-6',
    44: 'Degas Inhibited',
    45: 'IGC Inhibited',
    46: 'Brownout/Short',
    47: 'Service due'}
tic_snvt: dict[str, int] = {
    'PRESSURE': 59,  # float (Pascals only)
    'VOLTAGE': 66,  # float
    'PERCENT': 81}
tic_command_list: dict[str, int] = {
    'Device Off': 0,
    'Device On': 1,
    'Gauge Off': 0,
    'Gauge On': 1,
    'Gauge New_Id': 2,
    'Gauge Zero': 3,
    'Gauge Cal': 4,
    'Gauge Degas': 5,
    'Load Defaults': 576,
    'Upload': 0,
    'Download': 1}
tic_state: dict[int, str] = {
    0: 'Off',
    1: 'Off Going On',
    2: 'On Going Off Shutdown',
    3: 'On Going Off Normal',
    4: 'On'}
tic_active_gauge_state: dict[int, str] = {
    0: 'Gauge Not connected',
    1: 'Gauge Connected',
    2: 'New Gauge Id',
    3: 'Gauge Change',
    4: 'Gauge In Alert',
    5: 'Off',
    6: 'Striking',
    7: 'Initialising',
    8: 'Calibrating',
    9: 'Zeroing',
    10: 'Degassing',
    11: 'On',
    12: 'Inhibited'}
tic_full_pump_states: dict[int, str] = {
    0: 'Stopped',
    1: 'Starting Delay',
    2: 'Stopping Short Delay',
    3: 'Stopping Normal Delay',
    4: 'Running',
    5: 'Accelerating',
    6: 'Fault Braking',
    7: 'Braking'}
tic_gas_type: dict[str, int] = {
    'Nitrogen': 0,
    'Helium': 1,
    'Argon': 2,
    'Carbon Dioxide': 3,
    'Neon': 4,
    'Krypton': 5,
    'Voltage': 6}
tic_gas_type_by_id: dict[int, str] = dict(zip(tic_gas_type.values(), tic_gas_type.keys()))
tic_filter: dict[str, int] = {
    'Filter Off': 0,
    'Filter On': 1}
tic_filter_by_id: dict[int, str] = dict(zip(tic_filter.values(), tic_filter.keys()))
tic_gauge_type: dict[str, int] = {
    'Unknown Device': 0,
    'No Device': 1,
    'EXP_CM': 2,
    'EXP_STD': 3,
    'CMAN_S': 4,
    'CMAN_D': 5,
    'TURBO': 6,
    'APGM': 7,
    'APGL': 8,
    'APGXM': 9,
    'APGXH': 10,
    'APGXL': 11,
    'ATCA': 12,
    'ATCD': 13,
    'ATCM ': 14,
    'WRG': 15,
    'AIMC': 16,
    'AIMN': 17,
    'AIMS': 18,
    'AIMX': 19,
    'AIGC_I2R': 20,
    'AIGC_2FIL': 21,
    'ION_EB': 22,
    'AIGXS': 23,
    'USER': 24,
    'ASG': 25}
tic_gauge_type_by_id: dict[int, str] = dict(zip(tic_gauge_type.values(), tic_gauge_type.keys()))
tic_pump_tpye: dict[str, int] = {
    'No Pump': 0,
    'EXDC Pump': 1,
    'EXT75DX Pump': 3,
    'EXT255DX': 4,
    'Mains Backing Pump': 8,
    'Serial Pump': 9,
    'nEXT - 485': 10,
    'nEXT - 232': 11,
    'nXDS': 12,
    'Not yet identified': 0}
tic_pump_tpye_by_id: dict[int, str] = dict(zip(tic_pump_tpye.values(), tic_pump_tpye.keys()))
tic_operation: dict[str, str] = {
    'GENERAL COMMAND': '!C',
    'SETUP COMMAND': '!S',
    'QUERY SETUP': '?S',
    'QUERY VALUE': '?V'}


def tic_build_command(operation: str,
                      object_id: int,
                      *argv
                      ) -> str:
    """Build TIC6G command string from parameters."""
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(operation, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid operation.')
    if not isinstance(object_id, int):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid object_id.')
    # Example: !S913 0;0;0
    command: str = operation + str(object_id)
    cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: len(argv): {len(argv)} argv: {argv}')
    if len(argv) > 0:
        command += ' '
        for arg in argv:
            command += str(arg) + ';'
        command = command[:-1]
    cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: Command: {command}')
    return command


def tic_parse(response: str) -> list[str]:
    """
    Parse TIC6G response string.

    Parameters
    ----------
    response : str
        Resonse from TIC6G.

    Raises
    ------
    ValueError
        if TIC6G response is invalid.

    Returns
    -------
    (result_list: list(str), errno: int)
        List of TIC6G response parameters or error number.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(response, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid {response=}')
    cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: {response=}')
    if response[0] != '*' and response[0] != '=':  # Vaule returned
        raise ValueError(f'{tn}.{cn}.{fn}: Invalid value of {response=}')
    if response[0] == '*':  # Vaule returned
        status: str = re.split(' ', response, 1)
        raise Exception(f'{tn}.{cn}.{fn}: error={int(status[1])}.')
    result: str = re.split(' ', response, 1)
    result_list: list[str] = re.split(';', result[1])
    return result_list


async def tic_transaction(mqtt_client: aiomqtt.Client,
                          com_lock: asyncio.Lock,
                          com_interface: asyncio_com_interface.AsyncioComInterface,
                          command: str
                          ) -> str:
    """
    Perform low level write-read communication with device TIC6G.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    com_interface : asyncio_com_interface.AsyncioComInterface
        Interface connection to TIC6G device, serial or socket.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in tic_transaction. The default is None.
    command : str
        TIC6G command string.

    Returns
    -------
    response : str
        Response received from TIC6G.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(command, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid command.')
    if len(command) == 0:
        raise ValueError(f'{tn}.{cn}.{fn}: Invalid command.')
    cr: typing.Final[str] = '\r'
    async with com_lock:
        while True:
            try:
                cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: {command=}')
                write_task: asyncio.Task = asyncio.create_task(com_interface.write(command, cr), name=f'{tn}.{cn}.{fn}.write')
                read_task: asyncio.Task = asyncio.create_task(com_interface.read_until(cr), name=f'{tn}.{cn}.{fn}.read')
                try:
                    async with asyncio.timeout(10):
                        await write_task
                        response: str = await read_task
                        cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: {command=} -> {response=}')
                        return response
                except asyncio.TimeoutError as ex:
                    cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: Timeout exception cought. {ex}')
            except UnicodeDecodeError:
                cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: UnicodeDecodeError cought.')


async def tic_setup(mqtt_client: aiomqtt.Client,
                    com_lock: asyncio.Lock,
                    com_interface: asyncio_com_interface.AsyncioComInterface) -> list[tuple[int, int, int]]:
    """
    Perform setup of device TIC6G. Publish status to MQTT broker and send_status_mail task.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in tic_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface, optional
        Interface connection to TIC6G device, serial or socket. The default is None.

    Returns
    -------
    None

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Setup device TIC6G.')
    try:  # Query idn
        command: str = tic_build_command(tic_operation['QUERY SETUP'], tic_object['TIC Status'])
        response: str = await tic_transaction(mqtt_client, com_lock, com_interface, command)
        result_list: list[str] = tic_parse(response)
        cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: IDN Query {command=} -> {response=}')
        idn: str = ''
        for result in result_list:
            idn += result + '|'
        idn = idn[:-1]
        await mqtt_client.publish(cryo_globals.topic_prefix + '/tic/idn', payload=idn, retain=True)
        oid_gas_filter: list[tuple[int, int, int]] = [
            (tic_object['Gauge 1'], tic_gas_type['Argon'], tic_filter['Filter Off']),
            (tic_object['Gauge 2'], tic_gas_type['Nitrogen'], tic_filter['Filter Off']),
            (tic_object['Gauge 3'], tic_gas_type['Helium'], tic_filter['Filter Off']),
            (tic_object['Gauge 4'], tic_gas_type['Neon'], tic_filter['Filter Off'])]
        for oid, gas, f in oid_gas_filter:
            # Write command only. Set gauge setup - gas type (volt); filter on/off:  Config type = 7
            config_type: int = 7
            command = tic_build_command(tic_operation['SETUP COMMAND'], oid, config_type, gas, f)
            cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: Set gas type and filter. {command=}')
            response = await tic_transaction(mqtt_client, com_lock, com_interface, command)
            cryo_tic_logger.debug(f'Setup gauge. {command=} -> {response=}')
    except Exception as ex:
        raise Exception(f'{tn}.{cn}.{fn}: Setup device TIC6G failed.') from ex
    return oid_gas_filter


async def tic_readout_loop(mqtt_client: aiomqtt.Client,
                           wakeup_event: asyncio.Event,
                           com_lock: asyncio.Lock,
                           com_interface: asyncio_com_interface.AsyncioComInterface,
                           smtp_queue: asyncio.Queue | None = None
                           ) -> typing.NoReturn:
    """
    Perform periodic readout of device TIC6G. Publish status to MQTT broker and send_status_mail task.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    wakeup_event : asyncio.Event, optional
        Event used to wakeup periodic loop if tic_readout_interval was changed by MQTT. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in tic_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface, optional
        Interface connection to TIC6G device, serial or socket. The default is None.
    smtp_queue : asyncio.Queue, optional
        Queue to send status information to cryo.send_status_mail(). The default is None.

    Returns
    -------
    None

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(wakeup_event, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid wakeup_event.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(smtp_queue, asyncio.Queue | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid smtp_queue.')
    cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Using {com_interface}')
    try:
        oid_gas_filter: list[tuple[int, int, int]] = await tic_setup(mqtt_client, com_lock, com_interface)
        tic_iteration_counter: int = 0
        while True:  # Periodic readout
            tic_iteration_counter += 1
            tic_readout_time = datetime.datetime.now()
            gases: list[str] = []
            gauge_types: list[str] = []
            pressures: list[pint.Quantity | None] = []
            units: list[str | None] = []
            gauge_states: list[str | None] = []
            alerts: list[str | None] = []
            priorities: list[str | None] = []
            for oid, gas, _ in oid_gas_filter:
                gases.append(tic_gas_type_by_id[gas])
                # Write command and read response. 5: Read gauge type – e.g. AIMX: Config type = 5
                config_type = 5
                command: str = tic_build_command(tic_operation['QUERY SETUP'], oid, config_type)
                response: str = await tic_transaction(mqtt_client, com_lock, com_interface, command)
                result_list: list[str] = tic_parse(response)
                cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: Query gauge type. {command=} -> {response=} {result_list=}')
                gauge_type = tic_gauge_type_by_id[int(result_list[0])]
                gauge_types.append(gauge_type)
                cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: Query pressure value and status.')
                command = tic_build_command(tic_operation['QUERY VALUE'], oid)
                response = await tic_transaction(mqtt_client, com_lock, com_interface, command)
                result_list = tic_parse(response)
                cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: Query gauge status. {command=} -> {response=} {result_list=}')
                try:
                    pressure: pint.Quantity | None = float(result_list[0]) * ureg.Pa
                except IndexError:
                    pressure = None
                pressures.append(pressure)
                try:
                    unit: str | None = result_list[1]
                except IndexError:
                    unit = None
                units.append(unit)
                try:
                    gauge_state: str | None = tic_active_gauge_state[int(result_list[2])]
                except IndexError:
                    gauge_state = None
                gauge_states.append(gauge_state)
                try:
                    alert: str | None = tic_alert_id[int(result_list[3])]
                except IndexError:
                    alert = None
                alerts.append(alert)
                try:
                    priority: str | None = tic_priority[int(result_list[4])]
                except IndexError:
                    priority = None
                priorities.append(priority)
                cryo_tic_logger.info(f'{tn}.{cn}.{fn}: {tic_readout_time} {tic_object_by_id[oid]} ({tic_gas_type_by_id[gas]}) P={pressure:.3e} {unit}|{gauge_type}|{gauge_state}|{alert}|{priority}')
            if smtp_queue:
                info: dict[str, str | dict[str, datetime.datetime | list[pint.Quantity | None]]] = {
                    'task': 'tic',
                    'status': {
                        'readout_time': tic_readout_time,
                        'pressures': pressures}}
                await smtp_queue.put(info)
            try:
                await mqtt_client.publish(cryo_globals.topic_prefix + '/tic/iteration_counter', payload=tic_iteration_counter)
                await mqtt_client.publish(cryo_globals.topic_prefix + '/tic/readout_time', payload=str(tic_readout_time), retain=True)
                for ii, unit, gas, gauge_type, gauge_state, alert, priority in zip(range(len(pressures)), units, gases, gauge_types, gauge_states, alerts, priorities):
                    await mqtt_client.publish(cryo_globals.topic_prefix + '/tic/gauges/' + str(ii), f'{unit}|{gas}|{gauge_type}|{gauge_state}|{alert}|{priority}', retain=True)
                p_magnitudes: list[float] = []
                for ii, pressure in enumerate(pressures):
                    p_magnitude: float = pressure.to_base_units().magnitude
                    p_magnitudes.append(p_magnitude)
                    await mqtt_client.publish(cryo_globals.topic_prefix + '/pressures/' + str(ii), payload=p_magnitude, retain=True)
                await mqtt_client.publish(cryo_globals.topic_prefix + '/pressures', payload=json.dumps(p_magnitudes), retain=True)
            except Exception:
                pass
            await cryo.event_wait(wakeup_event, cryo_globals.tic_readout_interval)
    except Exception as ex:
        raise Exception(f'{tn}.{cn}.{fn}: Exception cought.') from ex


async def tic_subscribe_set_topics(mqtt_client: aiomqtt.Client) -> str:
    """
    Subscribe to MQTT topics belonging to devices tic.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    command_queue : asyncio.Queue
        Queue to send command from console to tic command handler.

    Returns
    -------
    subtopic: str.
        Subtopic to be used in match by caller.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    try:
        # Publish topics with default values
        await mqtt_client.publish(cryo_globals.topic_prefix + '/tic/readout_interval', payload=cryo_globals.tic_readout_interval.to_base_units().magnitude, retain=True)
        await mqtt_client.publish(cryo_globals.topic_prefix + '/tic/set_readout_interval', payload=cryo_globals.tic_readout_interval.to_base_units().magnitude, retain=False)
        # Subscribe to desired topics
        await mqtt_client.subscribe(cryo_globals.topic_prefix + '/tic/set_readout_interval')
        return 'tic'
    except Exception as ex:
        cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: Exception cought trying to publish readout_intervals: {ex}')


async def tic_handle_set_topics(mqtt_client: aiomqtt.Client,
                                wakeup_event: asyncio.Event,
                                cmd_queue: asyncio.Queue,
                                subtopic: str,
                                payload: str
                                ) -> None:
    """
    Handle MQTT topics belonging to devices tic.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker.
    wakeup_event : asyncio.Event
        Event to wakeup tic_readout_loop.
    command_queue : asyncio.Queue
        Queue to send command from console to tic command handler.
    subtopic : str
        Subtopic to be handled.

    Returns
    -------
    None

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(wakeup_event, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid wakeup_event.')
    if not isinstance(cmd_queue, asyncio.Queue):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid cmd_queue.')
    if not isinstance(subtopic, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid subtopic.')
    if not isinstance(payload, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid payload.')
    cryo_tic_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    cryo_tic_logger.info(f'{tn}.{cn}.{fn}: {subtopic}={payload}')
    try:
        match subtopic:
            case 'set_readout_interval':
                try:
                    cryo_globals.tic_readout_interval = float(payload) * cryo_globals.ureg.s
                    cryo_tic_logger.info(f'{tn}.{cn}.{fn}: tic_readout_interval={cryo_globals.tic_readout_interval}')
                    await mqtt_client.publish(cryo_globals.topic_prefix + '/tic/readout_interval', payload=cryo_globals.tic_readout_interval.to_base_units().magnitude, retain=True)
                    wakeup_event.set()
                except Exception as ex:
                    cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: Exception during conversion of tic_readout_interval. ({payload!r}). {ex}')
                    await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'Exception during conversion of tic_readout_interval. ({payload!r})')
            case _:
                pass
    except Exception as ex:
        cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: Exception cought trying to handle {subtopic}={payload!r} {ex}')


def tic_command_help() -> str:
    """Return help text for command line interface."""
    help_text: str = 'tic:\n' \
        '  gauge channel:int config_type:int\n'
    return help_text


async def tic_command_loop(mqtt_client: aiomqtt.Client,
                           com_lock: asyncio.Lock,
                           com_interface: asyncio_com_interface.AsyncioComInterface,
                           cmd_queue: asyncio.Queue
                           ) -> typing.NoReturn:
    """
    Handle commands received from queue.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in tic_transaction. The default is None.
    com_interface : asyncio_com_interface.AsyncioComInterface, optional
        Interface connection to TIC6G device, serial or socket. The default is None.
    cmd_queue : asyncio.Queue, optional
        Queue receiving command from console. The default is None.

    Returns
    -------
    None

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not mqtt_client:
        raise ValueError(f'{tn}.{cn}.{fn}: mqtt_client not specified.')
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(com_interface, asyncio_com_interface.AsyncioComInterface):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_interface.')
    if not isinstance(cmd_queue, asyncio.Queue):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid cmd_queue.')
    try:
        while True:
            cryo_tic_logger.debug('{tn}.{cn}.{fn}: wait for queue.')
            q_item: list[str] = await cmd_queue.get()
            try:
                match q_item[0]:
                    case 'gauge':
                        config_type = int(q_item[2])
                        match int(q_item[1]):
                            case 1:
                                oid = tic_object['Gauge 1']
                            case 2:
                                oid = tic_object['Gauge 2']
                            case 3:
                                oid = tic_object['Gauge 3']
                            case 4:
                                oid = tic_object['Gauge 4']
                            case 5:
                                oid = tic_object['Gauge 5']
                            case 6:
                                oid = tic_object['Gauge 6']
                            case _:
                                cryo_tic_logger.warning(f'{tn}.{cn}.{fn}: Invalid command parameter received. {q_item}')
                                continue
                        command: str = tic_build_command(tic_operation['GENERAL COMMAND'], oid, config_type)
                        cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Sending {command}')
                        response: str = await tic_transaction(mqtt_client, com_lock, com_interface, command)
                        cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Instrument response. {response}')
                        print(f'{tn}.{cn}.{fn}: Instrument response. {response}')
                    case _:
                        cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Invalid command received. {q_item}')
                        print(f'{tn}.{cn}.{fn}: Invalid command received. {q_item}')
            except IndexError:
                cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: IndexError cought.')
    except Exception as ex:
        cryo_tic_logger.exception('f{tn}.{cn}.{fn}: Exception cought.')
        raise Exception(f'{tn}.{cn}.{fn}: Exception cought.') from ex


async def tic_task(mqtt_client: aiomqtt.Client,
                   wakeup_event: asyncio.Event,
                   com_lock: asyncio.Lock,
                   interface: str,
                   cmd_queue: asyncio.Queue,
                   smtp_queue: asyncio.Queue | None = None
                   ) -> None:
    """
    Task to run coroutines belonging device TIC6G.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker
    wakeup_event : asyncio.Event, optional
        Event used to wakeup periodic loop if tic_readout_interval was changed by MQTT.
    com_lock : asyncio.Lock, optional
        Lock to proctect critical section in tic_transaction.
    interface : str, optional
        Interface to TIC6G device, serial or socket, eg. com3, /dev/ttyUSB0, host:port.
    cmd_queue : asyncio.Queue, optional
        Queue to send commands to E5025.
    smtp_queue : asyncio.Queue, optional
        Queue to send status information to cryo.send_status_mail().

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(wakeup_event, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid wakeup_event.')
    if not isinstance(com_lock, asyncio.Lock):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid com_lock.')
    if not isinstance(interface, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid interface.')
    if not isinstance(cmd_queue, asyncio.Queue):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid cmd_queue.')
    if not isinstance(smtp_queue, asyncio.Queue | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid smtp_queue.')
    cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Connect to KS700 at {interface}')
    connection_parameter: dict[str, bool | int | str] = {
        'interface': interface,
        'baudrate': 9600,
        'bytesize': aioserial.EIGHTBITS,
        'parity': aioserial.PARITY_NONE,
        'stopbits': aioserial.STOPBITS_ONE,
        'xonxoff': True}
    try:
        # await tic_reconnect_loop(mqtt_client=mqtt_client, wakeup_event=wakeup_event, com_lock=com_lock, interface=interface, max_reconnects=0, cmd_queue=cmd_queue, smtp_queue=smtp_queue)
        cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Using AsyncioComInterface context.')
        async with asyncio_com_interface.AsyncioComInterface.create(**connection_parameter) as com_interface:
            cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Using AsyncioComInterface connection established.')
            try:
                tasks: list[asyncio.Task] = []
                async with asyncio.TaskGroup() as tg:
                    tasks.append(tg.create_task(tic_readout_loop(mqtt_client, wakeup_event, com_lock, com_interface, smtp_queue=smtp_queue), name='tic_readout_loop'))
                    tasks.append(tg.create_task(tic_command_loop(mqtt_client, com_lock, com_interface, cmd_queue), name='tic_command_loop'))
                    for task in tasks:
                        task.add_done_callback(cryo.handle_task_done)
            except ExceptionGroup as ex:
                cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: ExceptionGroup cought in TaskGroup.')
                for i, exc in enumerate(ex.exceptions):
                    cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: ExceptionGroup: SubException {i + 1}: {exc}')
    except asyncio.CancelledError as ex:
        cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception.')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception.')
        raise asyncio.CancelledError(f'{tn}.{cn}.{fn}: asyncio.CancelledError cought. Re-raising exception.') from ex
    except ConnectionResetError as ex:
        cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception.')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception.')
        raise ConnectionResetError(f'{tn}.{cn}.{fn}: ConnectionResetError cought. Re-raising exception.') from ex
    except aioserial.SerialException as ex:
        cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception.')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception.')
        raise aioserial.SerialException(f'{tn}.{cn}.{fn}: aioserial.SerialException cought. Re-raising exception.') from ex
    except Exception as ex:
        cryo_tic_logger.exception(f'{tn}.{cn}.{fn}: Exception. Re-raising exception.')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: Exception cought. Re-raising exception.')
        raise Exception(f'{tn}.{cn}.{fn}: Execption cought. Re-raising exception.') from ex
    finally:
        cryo_tic_logger.info(f'{tn}.{cn}.{fn}: Done.')
        await utilities.publish_log_msg(mqtt_client, cryo_globals.topic_log_msg, f'{tn}.{cn}.{fn}: Done.')
