#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Monitor Artemis cryo system.

Migrated from: "D:\\Artemis\\Projektvorlage\\Application VI\\AmbientAll_FoV.vi"

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.dePumps
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import asyncio
import argparse
import datetime
from email.message import EmailMessage
import getpass
import inspect
import json
import logging.config
import logging.handlers
import os
import pathlib
import re
import sys
import typing
import pint
import aioconsole
import aiofiles
import aiomqtt
import aiosmtplib
import nest_asyncio
from pyartemis.cryo import cryo_globals
from pyartemis.cryo.cryo_globals import ureg
from pyartemis.common import exceptions
from pyartemis.common import utilities
from pyartemis.cryo import cryo_levels
from pyartemis.cryo import cryo_temperature
from pyartemis.cryo import cryo_pressure

cryo_logger = logging.getLogger("cryo")


def exception_handler(loop, context):
    """
    Define an exception handler that handles exceptions not handled otherwise.

    Parameters
    ----------
    loop : asyncio.unix_events._UnixSelectorEventLoop or
           asyncio.unix_events._WindowsSelectorEventLoop
        This is the running asyncio event loop.
    context : dict
        context of asyncio event loop.

    Returns
    -------
    None.

    """
    ex = context['exception']  # Get the exception
    cryo_logger.exception(f'exception_handler: Exception cought {ex}')
    sys.exit(-1)


async def event_wait(evt: asyncio.Event,
                     timeout: pint.Quantity
                     ) -> bool:
    """
    Wait for event with timeout.

    Parameters
    ----------
    evt : asyncio.Event
        Event to by waited on.
    timeout : float
        Time to wait for event set.

    Returns
    -------
    timedout : bool
        True if event timed out, else False.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(evt, asyncio.Event):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid evt.')
    if not isinstance(timeout, pint.Quantity):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid timeout.')
    cryo_logger.debug(f'{tn}.{cn}.{fn}: Wait for {evt}')
    try:
        timedout = False
        await asyncio.wait_for(evt.wait(), timeout.to_base_units().magnitude)
        cryo_logger.debug(f'{tn}.{cn}.{fn}: {evt} was set.')
    except asyncio.TimeoutError:
        timedout = True
        cryo_logger.debug(f'{tn}.{cn}.{fn}: {evt} timed out.')
    evt.clear()
    return timedout


def handle_task_done(task: asyncio.Task) -> None:
    """
    Handle task done callback.

    Parameters
    ----------
    task : asyncio.Awaitable
        Task that has been completed.
    mqtt_client : aiomqtt.Client, optional
        Client connection to MQTT broker. The default is None.

    Returns
    -------
    None.

    """
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(task, asyncio.Task):
        raise TypeError(f'{fn}: Invalid task.')
    cryo_logger.debug(f'\n{fn}: handle_task_done_0: {task.get_name()} is done: {task.done()}')
    try:
        if task.done():
            if isinstance(task.exception(), exceptions.ExitApplication):
                cryo_logger.debug(f'{fn}: handle_task_done_1: {task.get_name()} reports: {task.exception()}')
            elif isinstance(task.exception(), KeyboardInterrupt):
                cryo_logger.debug(f'{fn}: handle_task_done_2: {task.get_name()} was interrupted by <CTRL+C>)')
            else:
                cryo_logger.debug(f'{fn}: handle_task_done_3: {task.get_name()} was cancelled: {task.cancelled()}')
                if task.exception():
                    cryo_logger.debug(f'{fn}: handle_task_done_3: {task.get_name()} reports: {task.exception()}')
    except Exception as ex:
        cryo_logger.debug(f'{fn}: handle_task_done_4: {task.get_name()} Exception cought. {ex}')
    except asyncio.exceptions.CancelledError:
        cryo_logger.debug(f'{fn}: handle_task_done_5: {task.get_name()} was cancelled.')
    except BaseException as ex:
        cryo_logger.debug(f'{fn}: handle_task_done_6: {task.get_name()} BaseException cought. {type(ex)} {ex}')
        if isinstance(ex, ExceptionGroup):
            for i, exc in enumerate(ex.exceptions):
                cryo_logger.debug(f'{fn}: handle_task_done_7: {task.get_name()} ExceptionGroup: SubException {i + 1}: {exc}')


async def main(mqtt: dict[str, int | str],
               e5025_interface: str | None = None,
               ks2700_interface: str | None = None,
               tic_interface: str | None = None,
               email: str | None = None,
               ks2700_front: bool = False
               ) -> typing.NoReturn:
    """
    Main coroutine.

    Parameters
    ----------
    mqtt : dict{'broker': str, 'port': int, 'user': str, 'password': str}, optional
        aiomqtt.Client infos. The default is None.
    e5025_interface : str, optional
        E5025 interface name. The default is None.
    ks2700_interface : str, optional
        KS2700 interface name. The default is None.
    tic_interface : str, optional
        TIC  interface name. The default is None.
    email : str, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    None.

    """
    asyncio.current_task().set_name('CryoMonitor')
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt, dict):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt dict.')
    if not isinstance(e5025_interface, str | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid e5025_interface.')
    if not isinstance(ks2700_interface, str | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid ks2700_interface.')
    if not isinstance(tic_interface, str | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid tic_interface.')
    if not isinstance(email, str | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid email.')
    if not isinstance(ks2700_front, bool):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid ks2700_front.')
    print(f'Hello from {tn}.{cn}.')
    cryo_logger.info(f'{tn}.{cn}.{fn}: Refer to MQTT Broker {mqtt["broker"]} for status information.')
    loop = asyncio.get_running_loop()  # get the event loop
    loop.set_exception_handler(exception_handler)  # set the exception handler
    error: ExceptionGroup | None = None
    try:
        email_infos: dict | None = await read_email_configuration(email)
    except Exception:
        email_infos = None
    try:
        async with aiomqtt.Client(hostname=mqtt['broker'],
                                  port=mqtt['port'],
                                  username=mqtt['user'],
                                  password=mqtt['password'],
                                  will=aiomqtt.Will(topic=cryo_globals.topic_prefix,
                                                    payload='disconnected',
                                                    retain=True),
                                  clean_session=True
                                  ) as mqtt_client:
            await mqtt_client.publish(cryo_globals.topic_prefix, payload='online', retain=True)
            await mqtt_client.publish(cryo_globals.topic_prefix + '/log_msg', payload='', retain=True)
            try:
                e5025_wakeup_event: asyncio.Event = asyncio.Event()
                e5025_com_lock: asyncio.Lock = asyncio.Lock()
                ks2700_wakeup_event: asyncio.Event = asyncio.Event()
                ks2700_com_lock: asyncio.Lock = asyncio.Lock()
                tic_wakeup_event: asyncio.Event = asyncio.Event()
                tic_queue: asyncio.Queue = asyncio.Queue()
                tic_com_lock: asyncio.Lock = asyncio.Lock()
                if email_infos:
                    smtp_queue: asyncio.Queue | None = asyncio.Queue()
                else:
                    smtp_queue = None
                tasks: list[asyncio.Task] = []
                async with asyncio.TaskGroup() as tg:  # Add tasks for cryo monitoring here.
                    if e5025_interface:
                        tasks.append(tg.create_task(cryo_levels.e5025_task(mqtt_client=mqtt_client,
                                                                           wakeup_event=e5025_wakeup_event,
                                                                           com_lock=e5025_com_lock,
                                                                           interface=e5025_interface,
                                                                           smtp_queue=smtp_queue),
                                                    name='e5025'))
                    if ks2700_interface:
                        tasks.append(tg.create_task(cryo_temperature.ks2700_task(
                            mqtt_client=mqtt_client,
                            wakeup_event=ks2700_wakeup_event,
                            com_lock=ks2700_com_lock,
                            interface=ks2700_interface,
                            smtp_queue=smtp_queue,
                            ks2700_front=ks2700_front),
                            name='ks2700'))
                    if tic_interface:
                        tasks.append(tg.create_task(cryo_pressure.tic_task(
                            mqtt_client=mqtt_client,
                            wakeup_event=tic_wakeup_event,
                            com_lock=tic_com_lock,
                            interface=tic_interface,
                            cmd_queue=tic_queue,
                            smtp_queue=smtp_queue),
                            name='tic'))
                    if email_infos:
                        tasks.append(tg.create_task(send_status_mail(
                            smtp_queue=smtp_queue,
                            email_infos=email_infos),
                            name='email'))
                    # MQTT subscriber task is mandatory
                    tasks.append(tg.create_task(mqtt_subscriber_loop(
                        mqtt_client,
                        e5025_wakeup_event=e5025_wakeup_event,
                        ks2700_wakeup_event=ks2700_wakeup_event,
                        tic_wakeup_event=tic_wakeup_event),
                        name='mqtt_subscriber'))
                    # Consol input task is mandatory
                    tasks.append(tg.create_task(wait_for_console(
                        mqtt_client,
                        tic_queue=tic_queue),
                        name='console'))
                    for task in tasks:
                        task.add_done_callback(handle_task_done)
            except ExceptionGroup as ex:
                error = ex
                cryo_logger.exception(f'{tn}.{cn}.{fn}: ExceptionGroup cought in TaskGroup. {ex}')
                for i, exc in enumerate(ex.exceptions):
                    cryo_logger.exception(f'{tn}.{cn}.{fn}: ExceptionGroup: SubException {i + 1}: {exc}')
                raise ExceptionGroup(f'{tn}.{cn}.{fn}: ExceptionGroup cought in TaskGroup.', ex.exceptions) from ex
            except Exception as ex:
                error = ex
                cryo_logger.exception(f'{tn}.{cn}.{fn}: Exception cought in TaskGroup. {ex}')
                raise Exception(f'{tn}.{cn}.{fn}: Exception cought in TaskGroup.') from ex
            except BaseException as ex:
                error = ex
                cryo_logger.exception(f'{tn}.{cn}.{fn}: BaseException cought in TaskGroup. {ex}')
                raise Exception(f'{tn}.{cn}.{fn}: BaseException cought in TaskGroup.') from ex
            finally:
                await mqtt_client.publish(cryo_globals.topic_prefix, payload='offline', retain=True)
    except Exception as ex:
        cryo_logger.exception(f'{tn}.{cn}.{fn}: Stopping due to exception: {ex}')
        error = ex
    finally:
        try:
            error
        except NameError:
            cryo_logger.info(f'{tn}.{cn}.{fn}: No error to handle in finally.')
        else:
            cryo_logger.exception(f'{tn}.{cn}.{fn}: Handling error in finally: {error}')
            cryo_logger.exception(f'{tn}.{cn}.{fn}: Sending eMail with exception info.')
            await send_exit_mail(error, email_infos=email_infos)


async def mqtt_subscriber_loop(mqtt_client: aiomqtt.Client,
                               e5025_wakeup_event: asyncio.Event | None = None,
                               e5025_queue: asyncio.Queue | None = None,
                               ks2700_wakeup_event: asyncio.Event | None = None,
                               ks2700_queue: asyncio.Queue | None = None,
                               tic_wakeup_event: asyncio.Event | None = None,
                               tic_queue: asyncio.Queue | None = None
                               ) -> typing.NoReturn:
    """
    Subscribe to MQTT topics belonging to device E5025, KS2700 and TIC.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker. The default is None.
    e5025_wakeup_event : asyncio.Event, optional
        Event to wakeup e5025_readout_loop. The default is None.
    ks2700_wakeup_event : asyncio.Event, optional
        Event to wakeup ks2700_readout_loop. The default is None.
    tic_wakeup_event : asyncio.Event, optional
        Event to wakeup tic_readout_loop. The default is None.

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(e5025_wakeup_event, asyncio.Event | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid e5025_wakeup_event.')
    if not isinstance(e5025_queue, asyncio.Queue | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid e5025_queue.')
    if not isinstance(ks2700_wakeup_event, asyncio.Event | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid ks2700_wakeup_event.')
    if not isinstance(ks2700_queue, asyncio.Queue | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid ks2700_queue.')
    if not isinstance(tic_wakeup_event, asyncio.Event | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid tic_wakeup_event.')
    if not isinstance(tic_queue, asyncio.Queue | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid tic_queue.')
    cryo_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    # Publish and subscribe topic values
    e5025_subtopic: str = await cryo_levels.e5025_subscribe_set_topics(mqtt_client)
    ks2700_subtopic: str = await cryo_temperature.ks2700_subscribe_set_topics(mqtt_client)
    tic_subtopic: str = await cryo_pressure.tic_subscribe_set_topics(mqtt_client)
    while True:
        async for message in mqtt_client.messages:
            cryo_logger.debug(f'{tn}.{cn}.{fn}: {message.topic=}:{message.payload=!r}')
            # if isinstance(message.payload, bytes):
            #     payload_str: str = message.payload.decode('utf-8')
            # elif isinstance(message.payload, bytearray):
            #     payload_bytearry: bytearray = message.payload
            # elif isinstance(message.payload, str | int | float):
            #     payload_num: str | int | float = message.payload
            # else:
            #     payload_none: None = None
            payload: str = message.payload.decode('utf-8')
            topic_level: list[str] = re.split('/', str(message.topic))
            match topic_level[1]:
                case 'Cryo':
                    match topic_level[2]:
                        case _ if topic_level[2] == e5025_subtopic:
                            await cryo_pressure.tic_handle_set_topics(mqtt_client, tic_wakeup_event, tic_queue, topic_level[3], payload)
                        case _ if topic_level[2] == ks2700_subtopic:
                            await cryo_pressure.tic_handle_set_topics(mqtt_client, tic_wakeup_event, tic_queue, topic_level[3], payload)
                        case _ if topic_level[2] == tic_subtopic:
                            await cryo_pressure.tic_handle_set_topics(mqtt_client, tic_wakeup_event, tic_queue, topic_level[3], payload)
                        case _:
                            pass   # match topic_level[2]:
                case _:
                    pass  # match topic_level[1]:


async def read_email_configuration(full_filename: str) -> dict[str, str | list[str]] | None:
    """
    Read ome configuration information necessary to send anonymous email via smtp:25 from json file.

    Parameters
    ----------
    full_filename : os.PathLike    if not isinstance(mqtt_client, aiomqtt.Client):
            raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')

        Full path to eMail json configuration file.

    Returns
    -------
    infos : dict, e.g.
        {
        "From": "name@domain",
        "To": "name@domain",
        "Cc": ["name@domain", "name@domain"],
        "Bcc": ["name@domain", "name@domain"],
        "Subject": "Text for subject"
        }
        Some configuration information necessary to send anonymous email via smtp:25.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(full_filename, str):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid full_filename.')
    cryo_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    async with aiofiles.open(full_filename, mode='r') as f:
        json_content = await f.read()
        cryo_logger.debug(f'{tn}.{cn}.{fn}: eMail configuration from json file: {json_content}')
        try:
            infos: dict[str, str | list[str]] = json.loads(json_content)
            cryo_logger.debug(f'{tn}.{cn}.{fn}: eMail configuration from json: {infos}')
            cryo_logger.debug(f'{tn}.{cn}.{fn}: Subject: {infos["Subject"]}')
            cryo_logger.debug(f'{tn}.{cn}.{fn}: eMail From: {infos["From"]}')
            cryo_logger.debug(f'{tn}.{cn}.{fn}: eMail To: {infos["To"]}')
        except Exception as ex:
            cryo_logger.exception(f'{tn}.{cn}.{fn}: Exception cought: {ex}')
        return infos


async def send_status_mail(smtp_queue: asyncio.Queue | None = None,
                           email_infos: dict[str, str | list[str]] | None = None
                           ) -> typing.NoReturn:
    """
    Send anonymous email with some status information via smtp:25.

    Parameters
    ----------
    smtp_queue : asyncio.Queue, optional
        Queue receiving status information. The default is None.
    email_infos : dict, optional, e.g.
            {
            "From": "name@domain",
            "To": "name@domain",
            "Cc": ["name@domain", "name@domain"],
            "Bcc": ["name@domain", "name@domain"],
            "Subject": "Text for subject"
            }
        Some configuration information necessary to send anonymous email via smtp:25. The default is None.

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(smtp_queue, asyncio.Queue):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid smtp_queue.')
    if not isinstance(email_infos, dict | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid email_infos.')
    cryo_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    e5025_readout_time: datetime.datetime = datetime.datetime.now()
    ks2700_readout_time: datetime.datetime = datetime.datetime.now()
    tic_readout_time: datetime.datetime = datetime.datetime.now()
    helium_level: float | None = None
    nitrogen_level: float | None = None
    temperatures: list[float] | None = None
    pressures: list[float] | None = None
    first: bool = True
    send_time: datetime.datetime = datetime.datetime.now()
    if smtp_queue:
        while True:
            try:
                # print('tic_command_loop: wait for queue.')
                info = await smtp_queue.get()
                match info['task']:
                    case 'e5025':
                        try:
                            e5025_readout_time = info['status']['readout_time']
                            helium_level = info['status']['helium_level']
                            nitrogen_level = info['status']['nitrogen_level']
                            cryo_logger.debug(f'{tn}.{cn}.{fn}: e5025: helium_level={helium_level} nitrogen_level={nitrogen_level}')
                        except Exception as ex:
                            cryo_logger.exception(f'{tn}.{cn}.{fn}: e5025: Exception cought. {ex}')
                    case 'ks2700':
                        try:
                            ks2700_readout_time = info['status']['readout_time']
                            temperatures = info['status']['temperatures']
                            cryo_logger.debug(f'{tn}.{cn}.{fn}: ks2700: temperatures={temperatures}')
                        except Exception as ex:
                            cryo_logger.exception(f'{tn}.{cn}.{fn}: ks2700: Exception cought. {ex}')
                    case 'tic':
                        try:
                            tic_readout_time = info['status']['readout_time']
                            pressures = info['status']['pressures']
                            cryo_logger.debug(f'{tn}.{cn}.{fn}: tic: pressures={pressures}')
                        except Exception as ex:
                            cryo_logger.exception(f'{tn}.{cn}.{fn}: tic: Exception cought. {ex}')
                    case _:
                        pass
                if email_infos and helium_level and nitrogen_level and pressures:  # TODO: check for temperatures.
                    if first or divmod((datetime.datetime.now() - send_time).seconds, 60 * 60)[0] >= 6:  # Every 6 hours after start.
                        message = EmailMessage()
                        message['Subject'] = email_infos['Subject']
                        message['From'] = email_infos['From']
                        message['To'] = email_infos['To']
                        try:
                            message['Cc'] = ",".join(email_infos["Cc"])
                            message['Bcc'] = ",".join(email_infos["Bcc"])
                        except KeyError:
                            pass
                        content = 'Dear colleagues\n'
                        content += 'This is a test mail.\n\n'
                        content += 'Artemis Cryo Status\n'
                        if helium_level:
                            content += f'\nFilling levels at {e5025_readout_time}\n'
                            content += f'- Helium level = {helium_level * 100:.1f} %\n'
                            content += f'- Nitrogen level = {nitrogen_level * 100:.1f} %\n'
                        else:
                            content += '\nFilling levels not available.\n'
                        if temperatures:
                            content += f'\nTemperatures at {ks2700_readout_time}\n'
                            content += f'- 0 = {temperatures[0]}\n'
                            content += f'- 1 = {temperatures[1]}\n'
                            content += f'- 2 = {temperatures[2]}\n'
                            content += f'- 3 = {temperatures[3]}\n'
                        else:
                            content += '\nTemperatures are not available.\n'
                        if pressures:
                            content += f'\nPressures at {tic_readout_time}\n'
                            content += f'- 0 = {pressures[0]}\n'
                            content += f'- 1 = {pressures[1]}\n'
                            content += f'- 2 = {pressures[2]}\n'
                            content += f'- 3 = {pressures[3]}\n'
                        else:
                            content += '\nPressures are not available.\n'
                        content += '\nSent from main_cryo.py'
                        message.set_content(content)
                        cryo_logger.debug(f'{tn}.{cn}.{fn}: Sending eMail Message: {message}')
                        send_result = await aiosmtplib.send(message,
                                                            hostname='smtp.gsi.de',
                                                            port=25,
                                                            use_tls=False,
                                                            start_tls=None
                                                            )
                        cryo_logger.info(f'{tn}.{cn}.{fn}: {send_time} Result from sending status eMail: {send_result}')
                        send_time = datetime.datetime.now()
                        first = False
            except Exception as ex:
                cryo_logger.exception(f'{tn}.{cn}.{fn}: Exception cought. {ex}')


async def send_exit_mail(ex: BaseException,
                         email_infos: dict[str, str | list[str]] | None = None
                         ) -> None:
    """
    Send anonymous email with some exception information via smtp:25.

    Parameters
    ----------
    email_infos : dict, optional, e.g.
            {
            "From": "name@domain",
            "To": "name@domain",
            "Cc": ["name@domain", "name@domain"],
            "Bcc": ["name@domain", "name@domain"],
            "Subject": "Text for subject"
            }
        Some configuration information necessary to send anonymous email via smtp:25. The default is None.

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(email_infos, dict | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid email_infos.')
    cryo_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    send_time: datetime.datetime = datetime.datetime.now()
    try:
        if email_infos:
            message = EmailMessage()
            # message['Subject'] = email_infos['Subject']
            message['Subject'] = 'ARTEMIS: Cryo monitoring stopped.'
            message['From'] = email_infos['From']
            message['To'] = email_infos['To']
            try:
                message['Cc'] = ",".join(email_infos["Cc"])
                message['Bcc'] = ",".join(email_infos["Bcc"])
            except KeyError:
                pass
            content = 'Dear colleagues\n'
            content += 'pyartemis.main_cryo stopped with exception.\n\n'
            content += f'{ex}\n'
            if isinstance(ex, ExceptionGroup):
                for i, exc in enumerate(ex.exceptions):
                    content += f'SubException {i + 1}: {exc}\n'
            content += '\nSent from main_cryo.py'
            message.set_content(content)
            cryo_logger.debug(f'{tn}.{cn}.{fn}: Sending eMail Message: {message}')
            send_result = await aiosmtplib.send(message,
                                                hostname='smtp.gsi.de',
                                                port=25,
                                                use_tls=False,
                                                start_tls=None
                                                )
            cryo_logger.info(f'{tn}.{cn}.{fn}: {send_time} Result from sending exit eMail: {send_result}')
    except Exception as ex:
        cryo_logger.exception(f'{tn}.{cn}.{fn}: Exception cought. {ex}')


def setup_logging(configuration_file: str) -> None:
    """Set up logging from configuratin file."""
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(configuration_file, str):
        raise TypeError(f'{fn}: Invalid configuration_file.')
    cryo_logger.debug(f'{fn}: Enter....')
    try:
        config_file: os.PathLike = pathlib.Path(configuration_file)
        cryo_logger.info(f'{fn}: Configuring logging using {config_file}')
        with open(config_file, 'r', encoding='utf-8') as f_in:
            config = json.load(f_in)
        logging.config.dictConfig(config)
        # try:
        #     queue_handler = logging.getHandlerByName('queue_handler')
        #     if queue_handler is not None:
        #         queue_handler.listener.start()
        #         atexit.register(queue_handler.listener.stop)
        # except Exception as e:
        #     cryo_logger.exception(f'Exception cought when dealing witk queue_handler: {e}')
    except Exception as ex:
        print(f'{fn}: Exception cought. {ex}')


async def wait_for_console(mqtt_client: aiomqtt.Client,
                           tic_queue: asyncio.Queue | None = None
                           ) -> typing.NoReturn:
    """
    Wait for user input from console.

    Parameters
    ----------
    mqtt_client : aiomqtt.Client
        Client connection to MQTT broker. The default is None.
    tic_queue : asyncio.Queue, optional
        Queue to send command to tic_command_loop. The default is None.

    Raises
    ------
    exceptions
        exceptions.ExitApplication.

    Returns
    -------
    None.

    """
    tn: str = asyncio.current_task().get_name()
    cn: str = asyncio.current_task().get_coro().cr_code.co_name
    fn: str = inspect.currentframe().f_code.co_name
    if not isinstance(mqtt_client, aiomqtt.Client):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid mqtt_client.')
    if not isinstance(tic_queue, asyncio.Queue | None):
        raise TypeError(f'{tn}.{cn}.{fn}: Invalid tic_queue.')
    cryo_logger.debug(f'{tn}.{cn}.{fn}: Enter....')
    while True:
        user_input: str = await aioconsole.ainput('Enter command. (? or exit> ')
        command: list[str] = re.split(' ', user_input)
        match command[0]:
            case 'exit':
                if mqtt_client:
                    await mqtt_client.publish(cryo_globals.topic_prefix, payload='offline', retain=True)
                raise exceptions.ExitApplication(f'{tn}.{cn}.{fn}:Exit requested by user.')
            case '?':
                print('Select from following commands.', flush=True)
                if tic_queue:
                    print(cryo_pressure.tic_command_help(), flush=True)
            case 'tic':
                cryo_logger.info(f'{tn}.{cn}.{fn}: tic command reveived. {command}')
                if tic_queue:
                    await tic_queue.put(command[1:])
                else:
                    cryo_logger.warning(f'{tn}.{cn}.{fn}: Command ignored due to invalid tic queue.')
            case _:
                cryo_logger.warning(f'{tn}.{cn}.{fn}: : Invalid input.')


if __name__ == '__main__':
    # app_name = re.split('.py', os.path.basename(__file__))[0]
    print(os.path.basename(__file__) + """\n
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n""")
    parser = argparse.ArgumentParser(prog=os.path.basename(__file__),
                                     description='This is the Artemis Cryo Monitoring.',
                                     epilog='Default command: python -m pyartemis.main_cryo -b ee-raspi1001 --e5025 atpdev011:9002 --tic atpdev011:9001 --ks2700 atpdev011:9003 --ks2700_sens_con rear')
    parser.add_argument('-b', '--broker', dest='broker', required=True, default=None, help='MQTT Broker Node (Name or IP)')
    parser.add_argument('-p', '--port', dest='port', type=int, default=1883, help='MQTT Broker port (Default:1883)')
    parser.add_argument('--mqtt_user', dest='mqtt_user', default=None, help='MQTT Broker User Name')
    parser.add_argument('--top_level_topic', dest='top_level_topic', default='Artemis', help='MQTT top-level topic. Default is Artemis)')
    parser.add_argument('--e5025', dest='e5025_interface', default=None, help='Serial line interface to E5025 level measurement device.')
    parser.add_argument('--ks2700', dest='ks2700_interface', default=None, help='Serial line interface or ip:port to KS2700 temperature measurement device.')
    parser.add_argument('--ks2700_sens_con', dest='ks2700_sens_con', default='rear', help='KS2700 temperature sensor connection.(front or Default: rear)')
    parser.add_argument('--tic', dest='tic_interface', default=None, help='Serial line interface or ip:port to Pump and Pressure TIC Instrument Controller device.')
    parser.add_argument('--email', dest='email', default=None, help='Full path to eMail json configuration file')
    args = parser.parse_args()
    while True:
        answer: str = input('THIS IS BETA SOFTWARE! You will use it on your own risk!\nDo you want to continue (yes or default: no)? ')
        if re.match('yes', answer):
            break
        if len(answer) == 0 or re.match('no', answer):
            sys.exit(1)
    if sys.platform.lower() == "win32" or os.name.lower() == "nt":
        setup_logging(r'config\logging_stderr_json_file.json')
    else:
        setup_logging('config/logging_stderr_json_file.json')
    # setup_logging('config/logging_queued_stderr_json_file.json')
    mqtt_password: str | None = None
    if args.mqtt_user:  # but required
        print('Set password for MQTT')
        mqtt_password = getpass.getpass()
        if len(mqtt_password) == 0:
            sys.exit(-2)
    nest_asyncio.apply()
    # Change to the "Selector" event loop if platform is Windows
    if sys.platform.lower() == "win32" or os.name.lower() == "nt":
        from asyncio import set_event_loop_policy, WindowsSelectorEventLoopPolicy
        set_event_loop_policy(WindowsSelectorEventLoopPolicy())
    # Run your async application as usual
    mqtt_info: dict[str, int | str] = {'broker': args.broker,
                                       'port': args.port,
                                       'user': args.mqtt_user,
                                       'password': mqtt_password}
    try:
        asyncio.run(
            main(
                mqtt=mqtt_info,
                e5025_interface=args.e5025_interface,
                ks2700_interface=args.ks2700_interface,
                tic_interface=args.tic_interface,
                email=args.email,
                ks2700_front=bool(re.match('front', args.ks2700_sens_con.lower()))
            )
        )
    except BaseException as e:
        print(f'__main__: BaseException cought. {type(e)} {e}')
