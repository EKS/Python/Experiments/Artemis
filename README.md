# PyArtemis
PyArtemis contains a Python class library for Automation Control & Data Acquisition System for the
[Artemis experiment](https://www.gsi.de/en/work/research/appamml/atomic_physics/experimental_facilities/hitrap/experiments/artemis).

Python scripts within this project are migrated from exitsting [LabVIEW Code](https://git.gsi.de/EE-LV/Experiments/Artemis).

- Package name will be pyartemis
- Software archtecture will be asyncio or Pykka/PyAcdaq. That's not decided jet.
- Paho-MQTT will be used to publish process variables to network.
- Simple LabVIEW migration trials can be found in the directory migration.
- Simple asyncio trials  and other stuff can be found in the directory test.

This module uses its own logging handler for debug output:
logging.getLogger('artemis').addHandler(logging.NullHandler()) 

Related documents and information
=================================
- README.md
- LICENSE & eupl_v1.2_de.pdf & eupl_v1.2_en.pdf
- Contact: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
- Cone or Download: https://git.gsi.de/EKS/Python/Experiments/Artemis
- Bug reports: https://git.gsi.de/EKS/Python/Experiments/Artemis/-/issues
- Umbrello is used for UML diagram creation.

Dependencies
============
Refer to pyproject.toml.

Optional
-------

Acknowledgements
================
I'd like to thank D.Klein@gsi.de for help, especially with asyncio.

Known issues
============
- Agilient E5025 Digital Cryogen Monitor response does not contain valid values for nitrogen level.
- KEITHLEY KS2700 temperature measurment not jet tested.
- EDWARDS TIC Instrument Controller 6-Gauge pressure measurment not jet tested.

License
=======
Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de, bi.reich@gsi.de, a.krishnan@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Pint
----
Copyright (c) 2012 by Hernan E. Grecco and contributors.  See AUTHORS
for more details.

Some rights reserved.

Redistribution and use in source and binary forms of the software as well
as documentation, with or without modification, are permitted provided
that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the following
  disclaimer in the documentation and/or other materials provided
  with the distribution.

* The names of the contributors may not be used to endorse or
  promote products derived from this software without specific
  prior written permission.

THIS SOFTWARE AND DOCUMENTATION IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE AND DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

Release History
===============
Release 0.0.0.0
---------------
19.02.2024, H.Brand@gsi.de
- Add sending status eMail.
- Add UMl diagram.
15.02.2024, H.Brand@gsi.de
- Add nest-asyncio to enable debugging in Sypder IDE.
13.02.2024, H.Brand@gsi.de
- Add pressure measurement readout
- Exend streamlit-gui.
08.02.2024, H.Brand@gsi.de
- Add temperature measurement readout
31.01.2024, H.Brand@gsi.de
- Add level measurement readout
 H.Brand@gsi.de
- Create structued src folder.
17.01.2024, H.Brand@gsi.de
- Create this project.
